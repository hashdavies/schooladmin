import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import axios from 'axios';
import { confirmAlert } from 'react-confirm-alert'; 
import {LogoutNow} from './dependency/UtilityFunctions';

import * as serviceWorker from './serviceWorker';
    
import Routes from './routes';
import { BrowserRouter } from 'react-router-dom';
import { BASEURL } from './dependency/Config';
import {isSignedIn,getAccessToken} from '../src/auth';
require('react-js-vector-icons/fonts');

axios.defaults.baseURL = BASEURL;
  // axios.defaults.headers.common['Authorization'] = '';
  axios.defaults.headers.common['Authorization'] = getAccessToken();

//   axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  //  axios.defaults.headers.post['Content-Type'] = 'application/json';
//    axios.defaults.headers.post['Accept'] = 'application/json';
   axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    
//    Access-Control-Allow-Origin
  axios.interceptors.request.use(request => {
      console.log(request);
     return request;
  }, error => {
      console.log(error);
      return Promise.reject(error);
  });
  
  axios.interceptors.response.use(response => {
      console.log(response);
       return response;
  }, error => {

      // console.log(error);
      // return Promise.reject(error);
      if (401 === error.response.status) {
 
     confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui'>
            <h1>Are you sure?</h1>
            <p>Your Session has Expired, You will be Logout now</p>
            <button onClick={onClose}>No</button>
            <button
              onClick={() => {
                LogoutNow();
                 onClose();
              }}
            >
              Yes, Logout!
            </button>
          </div>
        );
      }
    });
    } else {
        // console.log(error);Ç    
        return Promise.reject(error);
}
  });

ReactDOM.render(
  <BrowserRouter>
      <Routes />
   </BrowserRouter>
  , document.getElementById('root'));
   serviceWorker.unregister();
 

 
 
