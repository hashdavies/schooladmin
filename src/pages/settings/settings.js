import React, { Component } from 'react';
import { MDBContainer, MDBBtn} from 'mdbreact';
import "react-tabs/style/react-tabs.css";
import "./settings.css";

class Addrole extends Component {

  render() {
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
      <div className="dashboardWrapper">
        <div className="displayFlex bold dash col2">
        <div className="equalWidth displayLeftFlex sg3">Settings</div>
        </div>
        <div className="displayLeftFlex">
        <form className="forms">
            <p className="textLeft">Set Token Expiration Period</p>
            <select className="browser-default custom-select styley">
            <option>---Select---</option>
            <option value="1">24hrs</option>
            <option value="2">10hrs</option>
            <option value="3">3hrs</option>
            </select>
            <MDBBtn type="button" className="btn-block z-depth-2 backgoundColor btnAdj">
                Save
            </MDBBtn>
        </form>
        </div>
        </div>
    </MDBContainer>
  );
}
}

export default Addrole;