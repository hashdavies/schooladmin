import React from "react";
import { BrowserRouter } from 'react-router-dom';
import { MDBNav, MDBNavItem } from "mdbreact";
import logo from "../../assets/img/logo.jpeg";
// import Mtn from "../../assets/img/mtn.png";
import "./nav.css";


export default () => (
  <BrowserRouter>

    <MDBNav>
      <MDBNavItem className="displayCenterFlex">
        <img src={logo} alt="logo" className="mtnLogo" />
      </MDBNavItem>
      {/* <MDBNavItem className="displayCenterFlex">
      <img src={Uba} alt="uba logo" className="ubaLogo" />
      </MDBNavItem> */}
      <MDBNavItem>
      </MDBNavItem>
      <MDBNavItem>
      </MDBNavItem>
    </MDBNav>

  </BrowserRouter>
);