import React from "react";
import {isSignedIn,getUsertype} from '../../auth';

import {Switch, } from 'react-router-dom'


import {
    Route,
    Redirect,
    BrowserRouter as Router,
    // Switch
  } from 'react-router-dom';
  import withRouterStore from '../../dependency/withRouterStore'
  import  routerStore from '../../stores/RouterStore'; 



import Dashboard from '../dashboard/dashboard';
// import UserManager from '../userManager/userManager';
import Activeschool from '../Schools/active';
import SingleSchool from '../Schools/SingleSchool';
import Newschool from '../Schools/newschool';
import Pending from '../Schools/pending';
import Activestudent from '../Student/active';
import Newstudent from '../Student/newschool';
import Pendingtudent from '../Student/pending';
import AllPayment from '../Payment/All';
import Features from '../Features/All';
import BillingPlan from '../BillingPlan/All';

import Login from '../login/login';
// import BankAdmin from '../userManager/bankAdmin';
// import Addrole from '../addRole/addrole';
// import AuditTrail from '../auditTrail/audittrail';
// import LinkCard from '../linkCard/linkcard';
// // import NewCardRequest from '../cardRequest/RequestList';
// import Settings from '../settings/settings';
// import CardRequest from '../CardRequestPage/CardRequestPage';
// import CardList from '../cardList/cardList';

// import NotFound from '../four04/notFound';
import InternalServer from '../five05/internalServer';
// import ChangePassword from '../changePassword/changePassword';

import SideNav, {NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';

import '@trendmicro/react-sidenav/dist/react-sidenav.css';

import "./sidebar.css";

const AuthService = {
    isAuthenticated: false,
    authenticate(cb) {
      this.isAuthenticated = true
      setTimeout(cb, 100)
    },
    logout(cb) {
      this.isAuthenticated = false
      setTimeout(cb, 100)
    }
  }
 
const SecretRoute = ({ component: Component, ...rest }) => (

    <Route {...rest} render={(props) => (
      isSignedIn() === true
        ? <Component {...props} />
        :
        <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
    )} />
  );

export default () => (
    <div className="bodyWrapper displayFlex justifyCenterFlex">

    <Router>
        
    <Route render={({ location, history }) => (  
        <React.Fragment>
            { isSignedIn()===true ?
            <div className="sideBar">
    <SideNav onSelect={(selected) => {
   
       const to = '/' + selected;
       if (location.pathname !== to) {
           history.push(to);
        // window.location.reload();
       }
    }}
    expanded={true}
>
    <SideNav.Toggle />
    
    <SideNav.Nav defaultSelected="/">

 
    <NavItem eventKey="">
          <NavIcon>
              <i className="fa fa-fw fa-home" />
          </NavIcon>
          <NavText>
              Dashboard
          </NavText>
          
      </NavItem>

      <NavItem eventKey="features">
          <NavIcon>
              <i className="fa fa-fw fa-home" />
          </NavIcon>
          <NavText>
              Features
          </NavText>
          
      </NavItem>

      <NavItem eventKey="BillingPlan">
          <NavIcon>
              <i className="fa fa-fw fa-home" />
          </NavIcon>
          <NavText>
          Billing Plan
          </NavText>
          
      </NavItem>
  
      <NavItem eventKey="userM">
          <NavIcon>
              <i className="fa fa-fw fa-user-plus" />
          </NavIcon>
          <NavText>
          School Management
          <div className="moreArrow"><i className="fa fa-fw fa-chevron-down" /></div>
          </NavText>
            
          <NavItem eventKey="activeSchool">
              <NavText>
                Active Schools
              </NavText>
          </NavItem>
          <NavItem eventKey="newschool">
              <NavText>
                New Schools
              </NavText>
          </NavItem>
          <NavItem eventKey="pendingschools">
              <NavText>
               Pending Schools
              </NavText>
          </NavItem>
          
      </NavItem>
      <NavItem eventKey="student">
          <NavIcon>
              <i className="fa fa-fw fa-user-plus" />
          </NavIcon>
          <NavText>
         Payment Management
          <div className="moreArrow"><i className="fa fa-fw fa-chevron-down" /></div>
          </NavText>
            
          <NavItem eventKey="allpayment">
              <NavText>
               All Payment
              </NavText>
          </NavItem>
          
         
          
      </NavItem>
    
      
      {/* <NavItem eventKey="student">
          <NavIcon>
              <i className="fa fa-fw fa-user-plus" />
          </NavIcon>
          <NavText>
          Student Management
          <div className="moreArrow"><i className="fa fa-fw fa-chevron-down" /></div>
          </NavText>
            
          <NavItem eventKey="activeStudent">
              <NavText>
                Active Student
              </NavText>
          </NavItem>
          <NavItem eventKey="newStudents">
              <NavText>
                New Students
              </NavText>
          </NavItem>
          <NavItem eventKey="DropOutstudent">
              <NavText>
               DropOut Students
              </NavText>
          </NavItem>
          
      </NavItem> */}
    
      
      
       
      {/* <NavItem eventKey="settings">
          <NavIcon>
              <i className="fa fa-fw fa-cog" />
          </NavIcon>
          <NavText>
              Settings
              <div className="moreArrow"><i className="fa fa-fw fa-chevron-down" /></div>
          </NavText>
          <NavItem eventKey="SettingsT">
              <NavText>
                  Token Expiration
              </NavText>
          </NavItem>
        
      </NavItem> */}
    </SideNav.Nav>
   
</SideNav>

</div>
:
null

}
 

            <div className="Dashboard">
            <Switch>
          <main>
            <Route exact path="/login" component={Login} />
            {/* <Route exact path="/forgetPasword/:token" component={Login} /> */}

            {/* component={withRouterStore(routerStore)(Post)} /> */}
           <React.Fragment>
           <SecretRoute exact path='/'   component={props => <Dashboard /> }/> 
           {/* <SecretRoute exact path='/activeSchool'   component={props => <Activeschool /> }/>  */}
           <SecretRoute exact path='/activeSchool'   component={withRouterStore(routerStore)(Activeschool)}/> 

           <SecretRoute exact path='/newschool'   component={props => <Newschool /> }/> 
           <SecretRoute exact path='/pendingschools'   component={props => <Pending /> }/> 
           
           {/* <SecretRoute exact path='/activeStudent'   component={props => <Activestudent /> }/>  */}
           <SecretRoute exact path='/activeStudent'   component={withRouterStore(routerStore)(Activestudent)}/> 
           <SecretRoute exact path='/newStudents'   component={props => <Newstudent /> }/> 
           <SecretRoute exact path='/DropOutstudent'   component={props => <Pendingtudent /> }/> 
           <SecretRoute exact path='/allpayment'   component={props => <AllPayment /> }/> 
           <SecretRoute exact path='/Features'   component={props => <Features /> }/> 
           <SecretRoute exact path='/BillingPlan'   component={props => <BillingPlan /> }/> 
           {/* <SecretRoute exact path='/SingleSchool'   component={props => <SingleSchool /> }/>  */}
           <SecretRoute exact path='/SingleSchool/:token' component={props => <SingleSchool />} />

           {/* <SecretRoute exact path='/CardList'   component={props => <CardList /> }/>  */}
           {/* <SecretRoute exact path='/SettingsT'   component={props => <Settings /> }/>  */}
           {/* <SecretRoute exact path='/ChangePass'   component={props => <ChangePassword /> }/>  */} 
            </React.Fragment>
         
        <SecretRoute exact path='/InternalServer'   component={props => <InternalServer/> }/> 
        </main>
        </Switch>

</div>
        </React.Fragment>
    )}
     />
</Router>  
 </div>
);
