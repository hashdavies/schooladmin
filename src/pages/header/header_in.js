import React, { Component } from "react";
import { BrowserRouter } from 'react-router-dom';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavbarToggler, MDBCollapse, MDBDropdown,
MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon } from "mdbreact";

import ls from 'local-storage'
import { confirmAlert } from 'react-confirm-alert'; 
import {LogoutNow,getfullname} from '../../dependency/UtilityFunctions';

import 'react-confirm-alert/src/react-confirm-alert.css'; 
 
 import logo from "../../assets/img/logo.jpeg";

import "./navv.css";
import { async } from "q";
class NavbarPage2 extends Component {
 
constructor(props) {
  super(props);
  this.state = {
    isOpen: false
  
  }
  this._logout=this._logout.bind(this);
    }

toggleCollapse = () => {
  this.setState({ isOpen: !this.state.isOpen });
}

_logout = async () => {
//   confirmAlert({
//     title: 'Logout',
//     message: 'Are you sure You Want To Logout',
//     buttons: [
//       {
//         label: 'Yes',
//         onClick: async() => {
//             const { history } = this.props;
//   let response= await ls.remove('Loggedinstate');
//  window.location.reload();
//         }
//       },
//       {
//         label: 'No',
//         onClick: () => {console.log("click")}
//       }
//     ]
//   });



  confirmAlert({
    customUI: ({ onClose }) => {
      return (
        <div className='custom-ui'>
          <h1>Are you sure?</h1>
          <p>You Will be logout of the system</p>
          <button onClick={onClose}>No</button>
          <button
            onClick={() => {
              LogoutNow();
               onClose();
            }}
          >
            Yes, Logout!
          </button>
        </div>
      );
    }
  });
  // const { history } = this.props;
 //alert('clicked');
//  let response= await ls.remove('Loggedinstate');
//  window.location.reload();
 //alert(response);
 //history.push('/');
  //return <Redirect to='/' />
}
 
render() {
  return (
    <BrowserRouter>
    <MDBNavbar  dark expand="md" className="blacknav">
      <MDBNavbarBrand className="displayCenterFlex alignCenterFlex">
      <MDBIcon fa icon="bars" className="barsFont" />
      <img src={logo} alt="logo" className="mtnLogo" />
      </MDBNavbarBrand>
      <MDBNavbarToggler onClick={this.toggleCollapse} />
      <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
        <MDBNavbarNav left>
        </MDBNavbarNav>
        <MDBNavbarNav right>
          <MDBNavItem>
            <MDBDropdown className='displayCenterFlex alignCenterFlex'>
            <div>{getfullname()}</div>
              <MDBDropdownToggle nav caret>
            
                <MDBIcon icon="user" />
                
              </MDBDropdownToggle>
              <MDBDropdownMenu className="dropdown-default" right>
                <MDBDropdownItem href="/ChangePass">Change Password</MDBDropdownItem>
                <MDBDropdownItem 
             onClick={this._logout}
                >Logout</MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavItem>
        </MDBNavbarNav>
      </MDBCollapse>
    </MDBNavbar>
    </BrowserRouter>
    );
  }
}

export default NavbarPage2;