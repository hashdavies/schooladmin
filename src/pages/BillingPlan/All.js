import React, { Component } from 'react';
import { MDBDataTable, MDBContainer } from 'mdbreact';
import { MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';
import Select from 'react-select';

import Datatable from 'react-bs-datatable'; // Import this package
import "react-tabs/style/react-tabs.css";
import "./../userManager/usermanager.css";
// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';


import DashboardStore from '../../stores/Dashboard'
import { ToastContainer, toast } from 'react-toastify';
import { observer } from 'mobx-react';
const options = [
  { value: 'chocolate', label: 'All Plan' },
  { value: 'strawberry', label: 'Basic' },
  { value: 'vanilla', label: 'Advance' }
]

@observer
class Features extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
    componentDidMount() {
    DashboardStore.GetAllPlans();
    DashboardStore.GetFeatureList();
    }


  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }


  render() {
    let displayStatus = '';
    if (DashboardStore.isprocessing) {
      displayStatus = 'Loading Please Wait....';
    }
    else {
      displayStatus = 'No Data to display';
    }
    const header = [

      { title: 'S/n', prop: 'id', sortable: true, filterable: true },
      { title: 'Bill Name', prop: 'name', sortable: true, filterable: true },
       { title: 'Feature', prop: 'feature' },
      // { title: 'Number of school', prop: 'schoolcount' },
      { title: 'Date Added', prop: 'date' },
      { title: 'Action ', prop: 'action', sortable: true },



    ];
    const body = [
      {
        id: '1',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '2',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '3',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '4',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '5',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '6',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '7',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '8',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '9',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '10',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },
      {
        id: '11',
        name: 'Starter',
        status: 'Available',
        feature: 'Customization,Result,SmS,etc',
        schoolcount: '22',
        date: '22,April 2018',
      },



    ];



    const customLabels = {
      first: '<<',
      last: '>>',
      prev: '<',
      next: '>',
      show: 'Display rows',
      entries: ' ',
      noResults: displayStatus,
    };
    return (
      <MDBContainer className="fullWidth alignCenterFlex dashContainer">
        <div className="dashboardWrapper">
          <ToastContainer />

          <div className="displayFlex bold dash col2">
            <div className="equalWidth displayLeftFlex sg3">App  Billing Plan</div>

            <div onClick={DashboardStore.ToggleModal} className="equalWidth displayRightFlex alignCenterFlex siteColor">
              <div className="displayFlex backgoundColor siteColor2" style={{ padding: 5, borderRadius: 5, cursor: 'pointer' }}>
                <i className="fa fa-fw fa-plus faStyle" />
                Add New Billing Plan</div></div>

          </div>
          <div className='tableWrapper'>
            <Datatable
              tableHeader={header}
              tableBody={DashboardStore.AllPlatformPlan}
              keyName="userTable"
              tableClass="striped hover responsive"
              rowsPerPage={5}
              rowsPerPageOption={[5, 10, 15, 20]}
              initialSort={{ prop: "username", isAscending: true }}
              // onSort={onSortFunction}
              labels={customLabels}
            />
          </div>
          <MDBModal isOpen={DashboardStore.Modalopen} toggle={DashboardStore.ToggleModal}>
            <MDBModalBody className="transparento">
              <form className="text-center form " style={{ background: '#fff' }} >
                <MDBModalHeader toggle={DashboardStore.ToggleModal} className="siteColor"> New Plan</MDBModalHeader>
                <MDBModalBody>
                  <div className="displayFlexSpaceBetween">
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Plan Name"
                      style={{ width: '48%' }}
                      name="PlanName"
                    value={DashboardStore.AddNewPlan.PlanName}
                    onChange={(e) => DashboardStore.handleInputChange(e, "AddNewPlan")}
                    />


                  </div>
                  <div className="centerFlex fullWidth selectArea">
                    <Select className='equal-width mb-4'
                      placeholder="Choose  Some Features"
                      isMulti
                      // value={selectedOption}
                      value={DashboardStore.AddNewPlan.Features} 
                      options={DashboardStore.FeatureList}
                      name="Features"
                    onChange={(value) => DashboardStore._handleChangeSelect(value, 'Features','AddNewPlan')}
                    />
                  </div>






                  <MDBBtn
                    type="submit"
                    className="btn-block z-depth-2 backgoundColor"
                    onClick={DashboardStore.isformedit ? DashboardStore._UpdatePlan :DashboardStore._AddNewPlan }

                  >

                    {DashboardStore.isprocessing ? 'Processing....' : 'Submit'}
                  </MDBBtn>
                </MDBModalBody>

              </form>

            </MDBModalBody>
          </MDBModal>


        </div>
      </MDBContainer>
    );
  }
}

export default Features;