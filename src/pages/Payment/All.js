import React, { Component } from 'react';
import { MDBDataTable, MDBContainer} from 'mdbreact';
import { MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';

import Datatable from 'react-bs-datatable'; // Import this package
import "react-tabs/style/react-tabs.css";
import "./../userManager/usermanager.css";
// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';

 
import DashboardStore from '../../stores/Dashboard'
import { ToastContainer, toast } from 'react-toastify';
import { observer } from 'mobx-react';


@observer
class CardRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    
  }
   
      
      toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
      
 
  render() {
    let displayStatus='';
    if(DashboardStore.isprocessing){
      displayStatus='Loading Please Wait....';
    }
    else{
      displayStatus='No Data to display';
    }
    const header = [
    
      { title: 'S/n', prop: 'id', sortable: true, filterable: true },
      { title: 'School Name', prop: 'name', sortable: true, filterable: true },
      { title: 'Student Name', prop: 'studentName'},
      { title: 'Amount', prop: 'amount'},
      { title: 'Date Added ', prop: 'date', sortable: true },
      { title: 'Status', prop: 'status', },
      { title: 'Transaction Id', prop: 'transaction'},

    ];
    const body = [
      {
        id: '1',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '2',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '3',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '4',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '5',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '6',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '7',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '8',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
       {
        id: '9',
        name: 'Tini Schools',
        studentName: 'Babadimeji Oluwaloni James',
        amount: '500,000',
        date: '22,dec 2018',
        status: 'Successful',
        transaction: '7575758585858',
       },
    
    ];
   
     
    
    const customLabels = {
      first: '<<',
      last: '>>',
      prev: '<',
      next: '>',
      show: 'Display rows',
      entries: ' ',
      noResults: displayStatus,
    };
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
      <div className="dashboardWrapper">
      <ToastContainer />

        <div className="displayFlex bold dash col2">
        <div className="equalWidth displayLeftFlex sg3">Payment History</div>
       
        <div onClick={DashboardStore.ToggleModal} className="equalWidth displayRightFlex alignCenterFlex siteColor">
              <div className="displayFlex backgoundColor siteColor2" style={{ padding: 5, borderRadius: 5, cursor: 'pointer' }}>
                <i className="fa fa-fw fa-plus faStyle" />
                Add Payment</div></div>
       
        </div>
        <div className='tableWrapper'> 
        <Datatable 
  tableHeader={header}
  tableBody={body}
  keyName="userTable"
  tableClass="striped hover responsive"
  rowsPerPage={5}
  rowsPerPageOption={[5, 10, 15, 20]}
  initialSort={{prop: "username", isAscending: true}}
  // onSort={onSortFunction}
  labels={customLabels}
/>Í
          </div>
          <MDBModal isOpen={DashboardStore.Modalopen} toggle={DashboardStore.ToggleModal}>
            <MDBModalBody className="transparento">
              <form className="text-center form " style={{ background: '#fff' }} >
                <MDBModalHeader toggle={DashboardStore.ToggleModal} className="siteColor">Add Payment</MDBModalHeader>
                <MDBModalBody>
                  <div className="displayFlexSpaceBetween twoForm">
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter School Name"
                      style={{ width: '48%' }}
                      name="School Name"
                      // value={DashboardStore.linkcard.firstName}
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Student Name"
                      style={{ width: '48%' }}
                      name="Student Name"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                  </div>
                  <div className="displayFlexSpaceBetween twoForm">

                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Amount"
                      style={{ width: '100%' }}
                      name="Amount"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                  </div>

                 
                 
                  


                  <MDBBtn
                    type="submit"
                    className="btn-block z-depth-2 backgoundColor"
                    
                  >

                    {DashboardStore.isprocessing ? 'Processing....' : 'Submit'}
                  </MDBBtn>
                </MDBModalBody>

              </form>

            </MDBModalBody>
          </MDBModal>
     
     
        </div>
    </MDBContainer>
  );
}
}

export default CardRequest;