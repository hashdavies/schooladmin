import React, { Component } from 'react';
import { MDBDataTable, MDBContainer} from 'mdbreact';
import { MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';
 import Datatable from 'react-bs-datatable'; // Import this package
import "react-tabs/style/react-tabs.css";
import "./../userManager/usermanager.css";
import 'react-web-tabs/dist/react-web-tabs.css';
// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';

 
import DashboardStore from '../../stores/Dashboard';
import { ToastContainer, toast } from 'react-toastify';
import { observer } from 'mobx-react';


@observer
class ParentInSchool extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    let SchoolId= DashboardStore.CurrentSchoolId;
   // alert(SchoolId);
    DashboardStore.GetAllParentInSchool()
  }
   
      
      toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
      
 
  render() {
     let displayStatus='';
    if(DashboardStore.isprocessing){
      displayStatus='Loading Please Wait....';
    }
    else{
      displayStatus='No Data to display';
    }
    const header = [
    
 
      { title: 'S/n', prop: 'id', sortable: true, filterable: true },
      { title: 'Name', prop: 'name', sortable: true, filterable: true },
      { title: 'Occupation', prop: 'occupation'},
      { title: 'Relationship', prop: 'Relationship'},
       { title: 'Email', prop: 'email', sortable: true },
      { title: 'Address', prop: 'address', },
       

    ];
    
    
    const customLabels = {
      first: '<<',
      last: '>>',
      prev: '<',
      next: '>',
      show: 'Display rows',
      entries: ' ',
      noResults: displayStatus,
    };
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
   
    
      <div className="dashboardWrapper">
      <ToastContainer />

        <div className="displayFlex bold dash col2">
        <div className="equalWidth displayLeftFlex sg3">Active Parents in {DashboardStore.CurrentSchoolName}</div>
      
        </div>
        <div className='tableWrapper'> 
        <Datatable 
  tableHeader={header}
  tableBody={DashboardStore.ParentInSchool}
  keyName="userTable"
  tableClass="striped hover responsive"
  rowsPerPage={10}
  rowsPerPageOption={[10, 20, 30,40]}
  initialSort={{prop: "username", isAscending: true}}
  // onSort={onSortFunction}
  labels={customLabels}
/>
          </div>
    
     
        </div>
    </MDBContainer>
  );
}
}

export default ParentInSchool;