import React, { Component } from 'react';
import { MDBDataTable, MDBContainer} from 'mdbreact';
import { MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';

import Datatable from 'react-bs-datatable'; // Import this package
import "react-tabs/style/react-tabs.css";
import "./../userManager/usermanager.css";
// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';

 
import DashboardStore from '../../stores/Dashboard'
import { ToastContainer, toast } from 'react-toastify';
import { observer } from 'mobx-react';


@observer
class CardRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    
  }
   
      
      toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
      
 
  render() {
    let displayStatus='';
    if(DashboardStore.isprocessing){
      displayStatus='Loading Please Wait....';
    }
    else{
      displayStatus='No Data to display';
    }
    const header = [
    
      { title: 'S/n', prop: 'id', sortable: true, filterable: true },
      { title: 'School Name', prop: 'name', sortable: true, filterable: true },
      { title: 'SubDomain', prop: 'SubDomain'},
      { title: 'Address', prop: 'Address'},
       { title: 'Date Join ', prop: 'date', sortable: true },
      { title: 'Mobile', prop: 'Mobile', },
      { title: 'Moto', prop: 'Moto'},

    ];
    const body = [
      {
        id: '1',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '2',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
      {
        id: '3',
        name: 'Tini Schools',
        SubDomain: 'tini.scloudschool.com',
        Address: '3, igi olugbin',
        date: '22,dec 2018',
        Mobile: '08093744884',
        Moto: 'Work and Pray',
       },
    
    ];
   
     
    
    const customLabels = {
      first: '<<',
      last: '>>',
      prev: '<',
      next: '>',
      show: 'Display rows',
      entries: ' ',
      noResults: displayStatus,
    };
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
      <div className="dashboardWrapper">
      <ToastContainer />

        <div className="displayFlex bold dash col2">
        <div className="equalWidth displayLeftFlex sg3">New School</div>
       
        {/* <div onClick={DashboardStore.ToggleModal} className="equalWidth displayRightFlex alignCenterFlex siteColor">
              <div className="displayFlex backgoundColor siteColor2" style={{ padding: 5, borderRadius: 5, cursor: 'pointer' }}>
                <i className="fa fa-fw fa-plus faStyle" />
                New Request</div></div> */}
       
        </div>
        <div className='tableWrapper'> 
        <Datatable 
  tableHeader={header}
  tableBody={body}
  keyName="userTable"
  tableClass="striped hover responsive"
  rowsPerPage={5}
  rowsPerPageOption={[5, 10, 15, 20]}
  initialSort={{prop: "username", isAscending: true}}
  // onSort={onSortFunction}
  labels={customLabels}
/>Í
          </div>
          <MDBModal isOpen={DashboardStore.Modalopen} toggle={DashboardStore.ToggleModal}>
            <MDBModalBody className="transparento">
              <form className="text-center form " style={{ background: '#fff' }} onSubmit={DashboardStore._SubmitCardRequest}>
                <MDBModalHeader toggle={DashboardStore.ToggleModal} className="siteColor">Request New Card</MDBModalHeader>
                <MDBModalBody>
                  <div className="displayFlexSpaceBetween twoForm">
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter First Name"
                      style={{ width: '48%' }}
                      name="firstName"
                      // value={DashboardStore.linkcard.firstName}
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Middle Name"
                      style={{ width: '48%' }}
                      name="middleName"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                  </div>
                  <div className="displayFlexSpaceBetween twoForm">

                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Surname"
                      style={{ width: '100%' }}
                      name="surName"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                  </div>

                  <input
                    type="text"
                    id="defaultFormCardPhoneEx"
                    className="form-control"
                    style={{ width: '100%', maxWidth: '100%' }}
                    placeholder="Phone Number"
                    name="phoneNumber"
                    onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                  />
                  <div className="displayFlexSpaceBetween twoForm">

                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Enter Your Location"
                      style={{ width: '100%' }}
                      name="location"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                    />
                  </div>
                  <div className="displayFlexSpaceBetween twoForm">

                    <textarea
                      type="text"
                      id="defaultFormContactMessageEx"
                      placeholder="Enter your Address"
                      className="form-control formCont linkCardInput"
                      rows="3"
                      name="address"
                      onChange={(e) => DashboardStore.handleInputChange(e, "cardrequest")}
                     />
                  </div>


                  <MDBBtn
                    type="submit"
                    className="btn-block z-depth-2 backgoundColor"
                    
                  >

                    {DashboardStore.isprocessing ? 'Processing....' : 'Submit'}
                  </MDBBtn>
                </MDBModalBody>

              </form>

            </MDBModalBody>
          </MDBModal>
     
     
        </div>
    </MDBContainer>
  );
}
}

export default CardRequest;