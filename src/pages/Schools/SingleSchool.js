import React, { Component } from 'react';
import { MDBDataTable, MDBContainer} from 'mdbreact';
import { MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';
import { TabProvider, Tab, TabPanel, TabList } from 'react-web-tabs';
import { withRouter } from 'react-router-dom'

import ParentInschool from './ParentInSchool';
import StudentInSchool from './StudentInSchool';
import TeacherInSchool from './TeacherInSchool';
import StaffsInSchool from './StaffsInSchool';
import withRouterStore from '../../dependency/withRouterStore'

import Datatable from 'react-bs-datatable'; // Import this package
import "react-tabs/style/react-tabs.css";
import "./../userManager/usermanager.css";
import 'react-web-tabs/dist/react-web-tabs.css';
// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';

 
import DashboardStore from '../../stores/Dashboard';
import  routerStore from '../../stores/RouterStore'; 
import { ToastContainer, toast } from 'react-toastify';
import { observer } from 'mobx-react';


@observer
class SingleSchool extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      loading:true
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    DashboardStore.GetAllActiveSchool()
     const  { params } =  this.props.match
    let token =params.token;
    //alert(token)
    DashboardStore.UpdateSchoolId(token)
this.setState({
  loading:false
})
  }
   
      
      toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
      
 
  render() {
   const {loading}=this.state;
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
      {loading===false ?

<TabProvider defaultTab="one">
<section className="my-tabs">
  <TabList className="my-tablist">
    <Tab tabFor="one">Students</Tab>
    <span className="divider">•</span>
    <Tab tabFor="two">Parent</Tab>
    <span className="divider">•</span>
    <Tab tabFor="three" className="my-tab">Teacher</Tab>
    <span className="divider">•</span>
    <Tab tabFor="four" className="my-tab">Staff</Tab>
  </TabList>
  <div className="wrapper">
    <TabPanel tabId="one">
    <StudentInSchool/>
    </TabPanel>
    <TabPanel tabId="two">
    <ParentInschool/> 
    </TabPanel>
    <TabPanel tabId="three">
     <TeacherInSchool/>
    </TabPanel>
    <TabPanel tabId="four">
<StaffsInSchool/>
    </TabPanel>
  </div>
</section>
</TabProvider>

      :
      null

      }
 
    </MDBContainer>
  );
}
}

 export default withRouter(SingleSchool);
//  export default withRouter(withRouterStore(routerStore)(SingleSchool));
//  export default withRouterStore(routerStore)(SingleSchool)