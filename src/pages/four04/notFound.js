import React, { Component } from 'react';
import { MDBBtn, MDBContainer, NavLink} from 'mdbreact';
import "./notFound.css";
class CardRequest extends Component {
 
  render() {
  return (
    <MDBContainer className="fullWidth alignCenterFlex displayCenterFlex fulllHeight">
      <div className="displayFlexColumn alignCenterFlex">
        <div className='font30'>Oops!</div>
        <div className='font40'>404 Not Found</div>
        <div className='requestErr'>Sorry, an error has occured, Requested page not found! </div>
       <NavLink to="/"> <MDBBtn type="button" className="btn-block z-depth-2 backgoundColor btnAdj goback">
                Go Back
            </MDBBtn></NavLink>
        </div>
    </MDBContainer>
  );
}
}

export default CardRequest;