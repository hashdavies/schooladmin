import React, { Component } from 'react';
// import React from "react";
import { Line } from "react-chartjs-2";
import { MDBContainer, MDBIcon} from 'mdbreact';
 import DashboardStore from '../../stores/Dashboard'

import { observer } from 'mobx-react';


 



import "./dashboard.css";
// import Uba from "../img/uba.svg";
// import Mtn from "../assets/img/mtn.png";
@observer
class FormPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLine: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
          {
            label: "",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [900, 1000, 1000, 500, 56, 55, 200]
          }
        ]
      }
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
  //  DashboardStore.GetPermission();
    //DashboardStore.Getcountry();
    // DashboardStore.GetRegions();
  }
    
      render() {
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
    <div className="dashboardWrapper">
        <div className="displayLeftFlex bold dash2 col2 sg3">Dashboard</div>
        <div className="d-flex bd-highlight example-parent flexWrap">
            <div className="p-2 flex-fill bd-highlight col-example">
              <div className="col2 ">Total No of Student</div>
              <div className="displayFlexSpaceAround">
                <div className="displayFlex alignCenterFlex">
                  <MDBIcon far icon="user" className="ml-2 incFont" />
                </div>
                <div>
                  <div className="fig">250</div>
                  <div className="col2 ">Student</div>
                </div>
              </div>
            </div>
            <div className="p-2 flex-fill bd-highlight col-example">
              <div className="col2 ">Total No of School</div>
                <div className="displayFlexSpaceAround">
                  <div className="displayFlex alignCenterFlex">
                    <MDBIcon far icon="paper-plane" className="ml-2 incFont" />
                  </div>
                  <div>
                    <div className="fig">150</div>
                    <div className="col2 ">School</div>
                  </div>
                </div>
            </div>
            <div className="p-2 flex-fill bd-highlight col-example">
            <div className="col2 ">Total No of Transaction Per Day</div>
                <div className="displayFlexSpaceAround">
                  <div className="displayFlex alignCenterFlex">
                    <MDBIcon far icon="id-card" className="ml-2 incFont" />
                  </div>
                  <div>
                    <div className="fig">50</div>
                    <div className="col2 ">Transaction</div>
                  </div>
                </div>
            </div>
            <div className="p-2 flex-fill bd-highlight col-example">
            <div className="col2 ">Total Amount Transacted</div>
                <div className="displayFlexSpaceAround">
                  <div className="displayFlex alignCenterFlex">
                    <MDBIcon far icon="credit-card" className="ml-2 incFont" />
                  </div>
                  <div>
                    <div className="fig">5,000,000</div>
                    <div className="col2 ">Amount</div>
                  </div>
                </div>
            </div>
        </div>
        {/* <MDBContainer>
          <div className="mt-5 displayLeftFlex bold col2">
          <span>linked card</span>
          <span className="date">jan 2018 - july 2018</span>
          
          </div>
          <Line data={this.state.dataLine} options={{ responsive: true }} />
        </MDBContainer> */}
    </div>
    </MDBContainer>
  );
}
};

export default FormPage;