import React, { Component } from 'react';
// import React from "react";
// import { BrowserRouter } from 'react-router-dom';
import { MDBContainer, MDBRow, MDBCol, MDBModal, MDBModalBody } from 'mdbreact';
import ls from 'local-storage';
import { ToastContainer, toast } from 'react-toastify';
import SimpleReactValidator from 'simple-react-validator';
import 'react-toastify/dist/ReactToastify.css';

import "./login.css";

import { reaction } from 'mobx';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react';





@observer
class LoginPage extends Component {
  // state = {

  //   }
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      email: '',
      password: '',
      rememberme: false,
      redirectToReferrer: false,
      errorloading: false,

    }
    this.validator = new SimpleReactValidator();
    AccountStore.Innit(this.validator)
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
    // AccountStore.Innit();
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }
    _LoginHandler = () => {
      // const{email,password,rememberme}=this.state;
      const { email, password, rememberme } = AccountStore.loginInfo;
      //   alert(email);
      if(email==='test@gmail.com' && password==='password'){
        let data={ 
          isloggedin: true,
          userid: 1,
          usertype: 'User',
         fullname:' School Admin',
          accessToken: 'Token',
        }
        let stringify_data=JSON.stringify(data);
        ls.set('Loggedinstate', data);

        toast.success("Login Successfully", {
          position: toast.POSITION.TOP_CENTER
        });
        this.setState(() => ({
          redirectToReferrer: true
        }))
      }
     else if(email==='' || password===''){
      toast.info("Enter Email and Password", {
        position: toast.POSITION.TOP_CENTER
      });
      }
      else{
       // alert('Incorrect Details.')
       toast.error("Incorrect Details", {
          position: toast.POSITION.TOP_CENTER
        });
      }
  //     // alert(rememberme)
  //     // fakeAuth.authenticate(() => {
  //     //   this.setState(() => ({
  //     //     redirectToReferrer: true
  //     //   }))
  //     // })
  //   }
  //   handleChange(e) {
  //     const value = e.target[e.target.type === "checkbox" ? "checked" : "value"]
  //     const name = e.target.name;

  //     // this.props.onFilter({
  //     //   [name]: value
  //     // });
  //     this.setState({
  //       [name]: value
  //     });
    }
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state
    const { history } = this.props;
    if (redirectToReferrer === true) {
      history.push(from);
          window.location.reload();

    }
    else{
      // alert("kk")
    }



    //     const { redirectToReferrer } = this.state

    // if (redirectToReferrer === true) {
    //   return <Redirect to='/' />
    // }
    return (
      <MDBContainer className="loginContainer displayCenterFlex alignCenterFlex bgImg">
        <MDBRow className="displayCenterFlex fullWidth">
          <MDBCol style={{ Width: 410, maxWidth: 410, }}>
          
            <form className="text-center border border-light formStyle formStyleee">

              <p className="h4 mb-4 siteColor">CloudSchool Admin Page</p>

              <p className="siteColor sg bold">Sign in</p>


              <input
                type="email"
                id="defaultSubscriptionFormPassword"
                class="form-control loginform-control mb-4"
                placeholder="E-mail"
                name="email"
                onChange={(e)=>AccountStore.handleInputChange(e,"loginInfo")}

                />


              <input
                type="password"
                id="defaultSubscriptionFormEmail"
                class="form-control loginform-control mb-4"
                placeholder="Password"
                name="password"
                 onChange={(e)=>AccountStore.handleInputChange(e,"loginInfo")}

                />

              <div class="custom-control custom-checkbox displayFlex my-4 bold ">
                <input
                  type="checkbox"
                  class="custom-control-input"
                  id="defaultRegisterFormNewsletter"
                  name="rememberme"
                  // onChange={AccountStore.handleInputChange} 
                  onChange={(e)=>AccountStore.handleInputChange(e,"loginInfo")}

                  />

                <label class="custom-control-label siteColor" for="defaultRegisterFormNewsletter">Remember Me</label>
              </div>


              <button
                class="btn backgoundColor btn-block"
                type="button"
                onClick={this._LoginHandler}
              >
              {AccountStore.isprocessing ? 'Processing....' : 'Sign in'} 
              
              </button>
              {/* <p onClick={this.toggle} className="siteColor bold noMargin fg">Forget Password?</p> */}


            </form>

          </MDBCol>
          <ToastContainer />
        </MDBRow>
        <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
          <MDBModalBody className="transparent"
          >
            <form className="text-center" onSubmit={AccountStore.onFormSubmit_ChangePwd}>

              <p className="siteColor2 sg bold">Reset your Password?</p>
             
      
      <div className="form-group">
      {/* <label>Title</label> */}

              <input 
              type="text" 
              id="defaultSubscriptionFormEmail" 
              class="form-control mb-4" 
              placeholder="E-mail Address"
              style={{ maxWidth: '100%' }} 
              value={AccountStore.resetpwd.email} 
              // onChange={this.setTitle}
              name="email"
              onChange={(e)=>AccountStore.handleInputChange(e,"resetpwd")}

               />
                  {AccountStore.validatorgen.message('email', AccountStore.resetpwd.email, 'required|email')}
                  </div>
              <button 
              class="btn backgoundColor2 btn-block bold" 
              type="submit">
              
              {AccountStore.isprocessing ? 'Processing....' : 'Continue'} 
              </button>
              <p onClick={this.toggle} className="siteColor2 bold fg">Back to login</p>


            </form>

          </MDBModalBody>
        </MDBModal>
      </MDBContainer>
    );
  }
};

export default LoginPage;