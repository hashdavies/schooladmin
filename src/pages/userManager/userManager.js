import React, { Component } from 'react';
import { MDBDataTable, MDBContainer, MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';
import Datatable from 'react-bs-datatable'; // Import this package

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import AccountStore from '../../stores/Account'
import Select from 'react-select';
import { ToastContainer, toast } from 'react-toastify';
import DataTable from 'react-data-table-component';
import { observer } from 'mobx-react';
import "react-tabs/style/react-tabs.css";
import "./usermanager.css";

@observer
class Usermanager extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    // this._LoginHandler = this._LoginHandler.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
     AccountStore.Getcountry();
    AccountStore.GetRoles_Bankuser();
    AccountStore.getCoperateUsers();
  }
    
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    let displayStatus='';
    if(AccountStore.isprocessing){
      displayStatus='Loading Please Wait....';
    }
    else{
      displayStatus='No Data to display';
    }
   
    const header = [
    
      { title: 'S/n', prop: 'id', sortable: true, filterable: true },
      { title: 'Full Name', prop: 'FullName', sortable: true, filterable: true },
      { title: 'Email Address', prop: 'EmailAddress', sortable: true, filterable: true },
      { title: 'Phone Number', prop: 'PhoneNumber'},
      { title: 'Country/Region', prop: 'CountryRegion'},
      { title: 'Role', prop: 'Role', sortable: true },
      { title: 'Action', prop: 'Action'},
    
    ];
    const customLabels = {
      first: '<<',
      last: '>>',
      prev: '<',
      next: '>',
      show: 'Display rows',
      entries: ' ',
      noResults: displayStatus,
    };
   
    
     
 



   
    
    return (
      <MDBContainer className="fullWidth alignCenterFlex dashContainer">
        <div className="dashboardWrapper">
        <ToastContainer />
          <div className="displayFlex bold dash col2">
            <div className="equalWidth displayLeftFlex sg3">User Management</div>
            <div onClick={()=>AccountStore.ToggleModal(true)} className="equalWidth displayRightFlex alignCenterFlex siteColor">
              <div className="displayFlex backgoundColor siteColor2" style={{ padding: 5, borderRadius: 5, cursor: 'pointer' }}>
                <i className="fa fa-fw fa-plus faStyle" />
                Add New User</div></div>
          </div>
          <div className='tableWrapper'> 
        <Datatable 
  tableHeader={header}
   

  tableBody={AccountStore.CorporateBranchUsers}
  keyName="CardList"
  tableClass="striped hover responsive"
  rowsPerPage={5}
  rowsPerPageOption={[5, 10, 15, 20]}
  initialSort={{prop: "username", isAscending: true}}
  // onSort={onSortFunction}
  labels={customLabels}
/>
          </div>
    
          <MDBModal isOpen={AccountStore.Modalopen} toggle={AccountStore.ToggleModal}>
            <MDBModalBody className="transparento">
              <form className="text-center form " style={{ background: '#fff' }} onSubmit={AccountStore.isformedit ? AccountStore.onFormSubmit_update_User :AccountStore._addBankUser  }>
                <MDBModalHeader toggle={AccountStore.ToggleModal} className="siteColor">{AccountStore.isformedit ? 'Update User Information' :'Add New User'  }</MDBModalHeader>
                <MDBModalBody>
                  <div className="displayFlexSpaceBetween twoForm">
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="First Name" 
                      style={{ width: '48%' }}
                      name="firstName"
                      value={AccountStore.Reg_BankUser.firstName}
                      onChange={(e) => AccountStore.handleInputChange(e, "Reg_BankUser")}
                    />
                    <input
                      type="text"
                      id="defaultFormCardNameEx"
                      className="form-control formControl3"
                      placeholder="Last Name"
                      style={{ width: '48%' }}
                      name="lastName"
                      value={AccountStore.Reg_BankUser.lastName}
                      onChange={(e) => AccountStore.handleInputChange(e, "Reg_BankUser")}
                    />
                  </div>
                  <input
                    type="email"
                    id="defaultFormCardEmailEx"
                    className="form-control"
                    placeholder="Email"
                    style={{ swidth: '100% !important', maxWidth: '100%' }}
                    name="email"
                    value={AccountStore.Reg_BankUser.email}

                    onChange={(e) => AccountStore.handleInputChange(e, "Reg_BankUser")}
                  />
                  <input
                    type="text"
                    id="defaultFormCardPhoneEx"
                    className="form-control"
                    placeholder="Phone Number"
                    name="phoneNumber"
                    value={AccountStore.Reg_BankUser.phoneNumber}

                    onChange={(e) => AccountStore.handleInputChange(e, "Reg_BankUser")}
                    style={{width: '100%', maxWidth: '100%'}}
                  />

                  <div className="centerFlex fullWidth selectArea">
                    <Select className='equal-width'
                      placeholder="Select a Country"
                      // value={selectedOption}
                      value={AccountStore.Reg_BankUser.country}
                      options={AccountStore.Allcountries}
                      name="Country"
 
                      onChange={(value) => AccountStore._handleChangeSelect(value, 'country', 'Reg_BankUser')}
                    />
                  </div>
                  <div className="centerFlex fullWidth selectArea">
                    <Select className='equal-width'
                      placeholder="Select a Region"
                      value={AccountStore.Reg_BankUser.region}
                      options={AccountStore.Allregions}
                      name="Region"
                      onChange={(value) => AccountStore._handleChangeSelect(value, 'region', 'Reg_BankUser')}
                    />
                  </div>
                  <div className="centerFlex fullWidth selectArea">
                    <Select className='equal-width'
                      placeholder="Select a Role"
                      value={AccountStore.Reg_BankUser.role}
                      options={AccountStore.Allroles}
                      name="Role"
                      onChange={(value) => AccountStore._handleChangeSelect(value, 'role', 'Reg_BankUser')}
                    />
                  </div>
                  {/* <input
                    type="text"
                    id="defaultFormCardTerminalEx"
                    className="form-control"
                    placeholder="Terminal ID"
                    style={{ swidth: '100% !important', maxWidth: '100%' }}
                    name="corporate"
                    onChange={(e) => AccountStore.handleInputChange(e, "Reg_BankUser")}
                  /> */}
                  <MDBBtn 
                  type="submit" 
                  className="btn-block z-depth-2 backgoundColor" 
                  // type="button"
                  // onClick={AccountStore._addBankUser}
                  >
                    
                    {AccountStore.isprocessing ? 'Processing....' : AccountStore.isformedit ? 'Update':'Add'} 
          </MDBBtn>
                </MDBModalBody>

              </form>

            </MDBModalBody>
          </MDBModal>
        
        
        </div>
      </MDBContainer>
    );
  }
}

export default Usermanager;