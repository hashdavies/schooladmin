import React, { Component } from 'react';
import { MDBDataTable, MDBContainer,MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader} from 'mdbreact';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import AccountStore from '../../stores/Account'
import Select from 'react-select';

import { observer } from 'mobx-react';
import { ToastContainer, toast } from 'react-toastify';

import "react-tabs/style/react-tabs.css";
import "./usermanager.css";
@observer
class BankAdmin extends Component {
    
      constructor(props) {
        super(props);
        this.state = {
          isloading:false,
          file: null
        }
        // this.handleChange = this.handleChange.bind(this)
        // this.handleChange = this.handleChange.bind(this);
      }
      componentDidMount(){
        // DashboardStore.GetPermission();
        AccountStore.Getcountry();
        AccountStore.GetRoles();
        // AccountStore.getCoperateUsers();
      }
      // handleChange(event) {
      //   event.preventDefault();
      //   this.setState({
      //     file: URL.createObjectURL(event.target.files[0])
      //   })
      // }
    triggerInputFile = () => this.fileInput.click();
 
  render() {
    let itemkeys=0;
//     const allCountry = AccountStore.allCountry.map((item)=>{ 
// // console.log(item)

// itemkeys+=1;
  
//         return (<option key={itemkeys.toString()} value="1">{item.countryName}</option>); 
//     }); 
  return (
    <MDBContainer className="fullWidth alignCenterFlex dashContainer">
                <ToastContainer />

      <div className="dashboardWrapper">
        <div className="displayFlex bold dash col2">
        <div className="equalWidth displayLeftFlex sg3">Create Bank Admin</div>
        
        </div>
        
  
        <form className="text-center form " style={{background:'#fff'}} onSubmit={AccountStore.onFormSubmit}>
            <div className="displayCenterFlex flexWrap">
                    <div className="centerFlex equalWidth2"> 
                                <input
                                type="text" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3  mb-4" 
                                placeholder="First Name" 
                                style={{width:'100%'}} 
                                name="firstName"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}

                                  />
                                <input type="text" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3  mb-4"
                                placeholder="Last Name"
                                style={{width:'100%'}}
                                name="lastName"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />
                                {/* </div> */}
                                <input type="email" 
                                id="defaultFormCardEmailEx" 
                                className="form-control formControl3  mb-4" 
                                placeholder="Enter Email Address"
                                style={{swidth: '100% !important',maxWidth: '100%'}}
                                name="email"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />
                                <input type="text" 
                                id="defaultFormCardPhoneEx"
                                className="form-control  mb-4" 
                                placeholder="Enter Phone number" 
                                style={{swidth: '100% !important',maxWidth: '100%'}}
                                name="phoneNumber"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />
                                <input 
                                type="text" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3  mb-4" 
                                placeholder="Enter The Company Name" 
                                style={{width:'100%'}}
                                name="companyName"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />

                                {/* <select className="browser-default custom-select">
                                <option>Country</option>
                                {allCountry}
                                </select> */}
                              <div className="centerFlex fullWidth selectArea">
                                  <Select className='equal-width mb-4'
                                      placeholder="Choose a Country"
                                      // value={selectedOption}
                                      value={AccountStore.Reg_BankAdmin.country} 
                                      options={AccountStore.Allcountries}
                                      name="Country"
                                      onChange={(value) => AccountStore._handleChangeSelect(value, 'country','Reg_BankAdmin')}
                                  />
                                </div>

                                {/* <div className="centerFlex fullWidth selectArea">
                    <Select className='equal-width'
                      placeholder="Select a Country"
                      // value={selectedOption}
                      value={AccountStore.Reg_BankUser.country}
                      options={AccountStore.Allcountries}
                      name="Country"
                      onChange={(value) => AccountStore._handleChangeSelect(value, 'country', 'Reg_BankUser')}
                    />
</div> */}

                                <div className="centerFlex fullWidth selectArea">
                                    <Select className='equal-width mb-4'
                                        placeholder="Select a Region"
                                        value={AccountStore.Reg_BankAdmin.region} 
                                        options={AccountStore.Allregions}
                                        name="Region"
                                        onChange={(value) => AccountStore._handleChangeSelect(value, 'region','Reg_BankAdmin')}
                                    />

                                </div>
                                <div className="centerFlex fullWidth selectArea">
                                    <Select className='equal-width mb-4'
                                        placeholder="Choose a Role"
                                        value={AccountStore.Reg_BankAdmin.role} 
                                        options={AccountStore.Allroles}
                                        name="Role"
                                        onChange={(value) => AccountStore._handleChangeSelect(value, 'role','Reg_BankAdmin')}
                                    />
                                </div>


                    
                                <input 
                                  type="text" 
                                  id="defaultFormCardNameEx" 
                                  className="form-control formControl3 mb-4" 
                                  placeholder="Enter Bank Title" 
                                  style={{width:'100%'}}
                                  name="BankTitle"
                                  onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                  />
                                <input 
                                type="text" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3 mb-4" 
                                placeholder="Enter Address" 
                                style={{width:'100%'}}
                                name="address"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />
                              <input 
                                type="email" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3 mb-4" 
                                placeholder="Enter Bank Email" 
                                style={{width:'100%'}}
                                name="bankemail"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}
                                />


                                  { 
                                      AccountStore.Reg_BankAdmin.showfield===true?
                                      <React.Fragment>
                                          <div className="centerFlex fullWidth selectArea">
                                            <Select className='equal-width mb-4'
                                                placeholder="Select Bank Country"
                                                // value={selectedOption}
                                                value={AccountStore.Reg_BankAdmin.BankCountry} 
                                                options={AccountStore.Allcountries}
                                                name="Bank Country"
                                                onChange={(value) => AccountStore._handleChangeSelect(value, 'BankCountry','Reg_BankAdmin')}
                                            />
                                          </div>
                                          <div className="centerFlex fullWidth selectArea">
                                            <Select className='equal-width mb-4'
                                                placeholder="Select Bank Region"
                                                value={AccountStore.Reg_BankAdmin.BankRegion} 
                                                options={AccountStore.Allregions2}
                                                name="Bank Region"
                                                onChange={(value) => AccountStore._handleChangeSelect(value, 'BankRegion','Reg_BankAdmin')}
                                            />

                                          </div>
                                      </React.Fragment>
                                        :
                                        null
                                    }

                            <div className="displayCenterFlex">
                              <input 
                                type="checkbox" 
                                id="defaultFormCardNameEx" 
                                className="form-control formControl3 " 
                                placeholder="Bank Title" 
                                style={{width:'5px',minWidth: '5px'}}
                                name="showfield"
                                onChange={(e)=>AccountStore.handleInputChange(e,"Reg_BankAdmin")}

                                /> 
                               <label style={{marginBottom:'0px'}} className="grey-text genFont">Show bank region </label>
                            </div>
                              
                          </div>

                    <div className="centerFlex equalWidth2">
                        <div className="equal-width2 leftFlex alignFlex">
                        <MDBBtn color="indigo btnAdjust2 bgColor3 removeTexttrans" type="button" onClick={this.triggerInputFile}>Browse Library</MDBBtn>
                        </div>
                        <input type='file' ref={fileInput => this.fileInput = fileInput} onChange={(e)=>AccountStore.handlefilePicker(e,"Reg_BankAdmin")}style={{display:"none",}} name="imageFile" />
                        <img className='priveiwImg' src={AccountStore.Reg_BankAdmin.imageFile} style={{    width: '80%', height: '100%', maxWidth: "300px", maxHeight: "230px", objectFit: 'cover',}}/>
                        
                  </div>
            </div>
              {/* <ToastContainer /> */}
              
              


          {/* <input type="text" id="defaultFormCardTerminalEx" className="form-control" placeholder="Terminal ID" style={{swidth: '100% !important',maxWidth: '100%'}}/> */}
          <MDBBtn
          //  type="button" 
          className="btn-block z-depth-2 backgoundColor" 
          type="submit"
          // onClick={AccountStore._addBankAdmin}
          >
            
            {AccountStore.isprocessing ? 'Processing....' : 'Add'} 

          </MDBBtn>
        {/* </MDBModalBody> */}

        </form>

        {/* </MDBModalBody>
      </MDBModal> */}
      </div>
    </MDBContainer>
  );
}
}

export default BankAdmin;