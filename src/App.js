import React, { Component } from 'react';
import Nav from './pages/header/header_in';
import Outnav from './pages/header/header_out';
import Side from './pages/header/sidebar';
 
import {isSignedIn} from '../src/auth';
 
import './App.css';           
// import accountStore from '../src/stores/Account'
//  import {observer} from 'mobx-react';
// import { reaction } from 'mobx';
 
// @observer
class App extends Component {
  constructor(props) {
    super(props); 

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  componentDidMount() {
let Logstate=isSignedIn();
this.setState({ signedIn: Logstate, checkedSignIn: true })
  }
  render() {

    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }
    return (
<React.Fragment>
{
  signedIn===true ?
<div className="App">
 <Nav/>
<Side/>
 

 {/* <div className="bodyWrapper displayFlex"> */}
{/* <div className="sideBar"><Side/></div> */}
{/* <div className="Dashboard"> */}
{/* <div>
  {accountStore.color}
</div> */}
{/* // {this.props.children} */}
{/* </div> */}
</div>
 
//  </div> 
:
<div className="App">
<Outnav/>
<Side/>
{/* <div className="bodyWrapper displayFlex justifyCenterFlex">

<div className="Dashboard">
 
{this.props.children}
</div>
</div> */}

 </div> 

}
</React.Fragment>

 
   
      

    );
  }
}

export default App;

