import React, { Component } from 'react';
import { MDBDataTable, MDBContainer, MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';

import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
import queryString from 'query-string'
import ls from 'local-storage';
import { ToastContainer, toast } from 'react-toastify';
import routerStore from './RouterStore'
import { ReadableDate } from '../dependency/UtilityFunctions';


import 'react-toastify/dist/ReactToastify.css';
import jwt_decode from 'jwt-decode';
import { isArray } from 'util';


class Dashboard {
    @observable AllActiveSchool = []
    @observable AllActiveStudent = []
    @observable AllFeatues = []
    @observable FeatureList = []
    @observable ParentInSchool = []
    @observable StaffsInSchool = []
    @observable StudentInSchool = []
    @observable TeacherInSchool = []
    @observable AllPlatformPlan = []
    @observable CurrentFeatureId ='';
    @observable CurrentPlanId ='';
    @observable isformedit =false;


    @observable AllCardList = []
    @observable loginInfo = {
        email: '',
        password: '',
        rememberme: false,
    }
    @observable AddFeature = {
        FeatureName: '',
   
    }
    @observable AddNewPlan = {
        PlanName: '',
        Selected_features: [],
        Features:[],
   
    }





    // @observable cardrequest = {
    //     phoneNumber: "",
    //     surName: "",
    //     firstName: "",
    //     middleName: "",
    //     location: "",
    //     address: ""
    // }
    // @observable linkcard = {
    //     expiryDate: '',
    //     cvv: '',
    //     panNumber: '',
    //     reference: '',
    //     card_id: '',

    // }
    // @observable AllUserRoles = []
    // @observable CompanionPermissions = []



    @observable loggedinUserdetails = {

    }
    @observable Modalopen = false;
    @observable CurrentSchoolId =null;
    @observable CurrentSchoolName =null;
    @observable disabled = false
    @observable loader_visible = false
    @observable redirectmode = false
    @observable isprocessing = false
    @observable redirectToReferrer = false

    @action ToggleModal(edit=false) {
        if(edit===true){
            DashboardStore.isformedit=true;
        }
        else{
            DashboardStore.isformedit=false;

        }
        DashboardStore.Modalopen = !DashboardStore.Modalopen;
        console.log(DashboardStore.Modalopen);
        if(DashboardStore.Modalopen===false){
            DashboardStore.ResetFeatureForm();

        }
    }
    @action UpdateSchoolId(CurrentSchoolId) {
        DashboardStore.CurrentSchoolId = CurrentSchoolId;
    }
    @action _handleChangeSelect = (selectedOption, name, type) => {
        let value = selectedOption.value;
console.log(value)
this.AddNewPlan.Selected_features = [];

selectedOption.forEach(item => {
    console.log(item['value']);
    console.log(item['label']);
    let value = item['value'];
    this.AddNewPlan.Selected_features.push(value);
});
    this[type][name] = selectedOption;
    console.table(this.AddNewPlan.Selected_features);

        // let expiryDate = stateOption.value.expiryDate;
        // let cvv = stateOption.value.cvv;
        // let panNumber = stateOption.value.panNumber;
        // let reference = stateOption.value.reference;
        // let card_id = stateOption.value.id;
        // this[type]['expiryDate'] = expiryDate;
        // this[type]['cvv'] = cvv;
        // this[type]['panNumber'] = panNumber;
        // this[type]['reference'] = reference;
        // this[type]['card_id'] = card_id;

    }

    @action handleInputChange(e, type) {
        const value = e.target[e.target.type === "checkbox" ? "checked" : "value"]
        const name = e.target.name;
        console.log(name)
        console.log(type)
        console.log(value)
        DashboardStore[type][name] = value;
    }
    @action _ToggleLoader = (status) => {
        this.loader_visible = status;
    }
    @action _ToggleProcessing = () => {
        this.isprocessing = !this.isprocessing;
    }
  

    @action _AddNewFeature(e) {
        e.preventDefault()
        console.log(DashboardStore.AddFeature)

        const {
            FeatureName,
        } = DashboardStore.AddFeature;
      
        console.log(DashboardStore.AddFeature)

        if (FeatureName === '') {
            toast.warning("Feature name is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else {


            let params = {
                name:FeatureName,
            }

            console.log(params);
            DashboardStore.isprocessing = true
            axios.post(`features`, params)
                .then(response => {
                    DashboardStore.isprocessing = false
                   // console.log(response)
                    //console.log(response.status)

                    if (response.status === 200 || response.status === 201) {
                        // console.log(resp)
                        let resp = response.data;
                        toast.success('Features Created', {
                            position: toast.POSITION.TOP_CENTER
                        });
                        DashboardStore.GetAllFeatures()
                        DashboardStore.ToggleModal();
                 }
                    else {
                        console.log(response)
                        DashboardStore.isprocessing = false
                    }

                })
                .catch(err => {
                    DashboardStore.isprocessing = false
                    toast.error('An Error Occured \n Please Check the Email Address', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }

    @action _AddNewPlan(e) {
        e.preventDefault()
        console.log(DashboardStore.AddFeature)

        const {
            Selected_features,
            PlanName,
        } = DashboardStore.AddNewPlan;
       
        console.log(DashboardStore.AddNewPlan)

        if (PlanName === '') {
            toast.warning("Plan name is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else if (Selected_features.length < 1) {
            toast.warning("Select Some Features for this plan", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else {
// alert("po")
let string_feature=JSON.stringify(Selected_features);

            let params = {
                name:PlanName,
                feature_name:string_feature,
            }

            console.log(params);
            DashboardStore.isprocessing = true
            axios.post(`plans`, params)
                .then(response => {
                    DashboardStore.isprocessing = false
                   // console.log(response)
                    //console.log(response.status)

                    if (response.status === 200 || response.status === 201) {
                        // console.log(resp)
                        let resp = response.data;
                        toast.success('New Plan  Created', {
                            position: toast.POSITION.TOP_CENTER
                        });
                        DashboardStore.GetAllPlans();

                        DashboardStore.ToggleModal();
                 }
                    else {
                        console.log(response)
                        DashboardStore.isprocessing = false
                    }

                })
                .catch(err => {
                    DashboardStore.isprocessing = false
                    toast.error('An Error Occured', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }
    @action _UpdatePlan(e) {
       // alert("update")
        e.preventDefault()
        console.log(DashboardStore.AddNewPlan)
        let C_P_id=DashboardStore.CurrentPlanId;

       // CurrentPlanId
        const {
            Selected_features,
            PlanName,
        } = DashboardStore.AddNewPlan;
       
        console.log(DashboardStore.AddNewPlan)

        if (PlanName === '') {
            toast.warning("Plan name is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else if (Selected_features.length < 1) {
            toast.warning("Select Some Features for this plan", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else {
// alert("po")
let string_feature=JSON.stringify(Selected_features);

            let params = {
                name:PlanName,
                feature_name:string_feature,
            }

            console.log(params);
            DashboardStore.isprocessing = true
            axios.patch(`plans?id=${C_P_id}`, params)
                .then(response => {
                    DashboardStore.isprocessing = false
                   // console.log(response)
                    //console.log(response.status)

                    if (response.status === 200 || response.status === 201) {
                        // console.log(resp)
                        let resp = response.data;
                        toast.success('New Plan  Created', {
                            position: toast.POSITION.TOP_CENTER
                        });
                        DashboardStore.GetAllPlans();
                        DashboardStore.ResetFeatureForm()
                        DashboardStore.ToggleModal();
                 }
                    else {
                        console.log(response)
                        DashboardStore.isprocessing = false
                    }

                })
                .catch(err => {
                    DashboardStore.isprocessing = false
                    toast.error('An Error Occured', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }

    @action _Feature_update(e) {
        e.preventDefault()
        // alert("edit form")
        console.log(DashboardStore.AddFeature)
let C_f_id=DashboardStore.CurrentFeatureId;
// alert(C_f_id);
        const {
            FeatureName,
        } = DashboardStore.AddFeature;
      
        console.log(DashboardStore.AddFeature)

        if (FeatureName === '') {
            toast.warning("Feature name is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
       else {


            let params = {
                // id:C_f_id,
                name:FeatureName,
            }

            console.log(params);
            DashboardStore.isprocessing = true
            axios.patch(`features?id=${C_f_id}`, params)
                .then(response => {
                    DashboardStore.isprocessing = false
                    // console.log(response)
                    //console.log(response.status)

                    if (response.status === 200 || response.status === 201) {
                        // console.log(resp)
                        let resp = response.data;
                      //  alert(JSON.stringify(resp))
                        // let internalcode = resp.code;

                        // let text = resp.description;
                        toast.success('Features Updated', {
                            position: toast.POSITION.TOP_CENTER
                        });

                        // if (internalcode > 0) {
                        //     toast.success(text, {
                        //         position: toast.POSITION.TOP_CENTER
                        //     });
                        DashboardStore.GetAllFeatures()
                        DashboardStore.ResetFeatureForm()
                        
                        DashboardStore.ToggleModal();
                        // }
                        // else {
                        //     toast.error(text, {
                        //         position: toast.POSITION.TOP_CENTER
                        //     });
                        // }




                    }
                    else {
                        console.log(response)
                        DashboardStore.isprocessing = false
                    }

                })
                .catch(err => {
                    DashboardStore.isprocessing = false
                    toast.error('An Error Occured \n Please Check the Email Address', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }

    GetAllFeatures = () => {
        console.warn("features");
        DashboardStore._ToggleProcessing();

        axios.get("features")
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);

                        // { title: 'S/n', prop: 'id', sortable: true, filterable: true },
                        // { title: 'Feature Name', prop: 'name', sortable: true, filterable: true },
                        // { title: 'Status', prop: 'status'},
                        // { title: 'Date Added', prop: 'date'},
                        // { title: 'Access ', prop: 'access', sortable: true },
                        // { title: 'Action ', prop: 'action', sortable: true },
                        return {                        
                            id:index++,
                            name: singleData.name,
                            date: Requestdate,
                            action: <div className="displayCenterFlex">
                                <MDBIcon fa icon="edit" className="ml-2 lightweight" onClick={() => DashboardStore.UpdateFeature(singleData)} />
                                {/* <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /> */}
                                {/* <MDBIcon onClick={() => DashboardStore.ModalNewuser(singleData)} fa icon="fa-plus" className="ml-2 colred lightweight" />  <i className="fa fa-fw fa-plus faStyle" onClick={() => DashboardStore.ModalNewuser(singleData)} />*/}
                                </div> ,
                        };
                      });
// console.log(formattedMessagedata);
                    this.AllFeatues = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
 
                    this.AllFeatues = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.AllFeatues = [];
                //  this._ToggleLoader(false)
            });
    }

    GetFeatureList = () => {
        console.warn("features");
        DashboardStore._ToggleProcessing();

        axios.get("features")
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);
 
                        return {   
                            id:index++,
                              value: singleData.name, 
                             label: singleData.name,
                             };
                      });
console.log(formattedMessagedata);
                    this.FeatureList = formattedMessagedata

                 }
                else {
                    DashboardStore._ToggleProcessing();
 
                    this.FeatureList = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.FeatureList = [];
                //  this._ToggleLoader(false)
            });
    }

    GetAllPlans = () => {
        console.warn("features");
        DashboardStore._ToggleProcessing();

        axios.get("plans")
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }

                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);
                        let featureLists=null;
                        if(this.isJson(singleData.feature_name)){
                            let feature_array=JSON.parse(singleData.feature_name);
                            // console.log(feature_array)
featureLists=this.toCommaSeperated(feature_array);
                        }
                        else{
                            // console.log("jiji")
                            featureLists=singleData.feature_name;
                        }
                        // isJson

                        return {   
                            id:index++,
                              name: singleData.name, 
                              feature: featureLists,
                              date: Requestdate,
                              action: <div className="displayCenterFlex">
                              <MDBIcon fa icon="edit" className="ml-2 lightweight" onClick={() => DashboardStore.Updateplan(singleData)} />
                              {/* <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /> */}
                              {/* <MDBIcon onClick={() => DashboardStore.ModalNewuser(singleData)} fa icon="fa-plus" className="ml-2 colred lightweight" />  <i className="fa fa-fw fa-plus faStyle" onClick={() => DashboardStore.ModalNewuser(singleData)} />*/}
                              </div> ,
                        };
                      });
console.log(formattedMessagedata);
                    this.AllPlatformPlan = formattedMessagedata

                 }
                else {
                    DashboardStore._ToggleProcessing();
 
                    this.AllPlatformPlan = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.AllPlatformPlan = [];
                //  this._ToggleLoader(false)
            });
    }





















 
    @action navigate2school = (data) => {
        // alert("hey")
        console.log(data)
    //  alert(JSON.stringify(data))
    let Schoolname=data.name;

     let schoolid=data.id;
     this.CurrentSchoolName=Schoolname;
    //  window.location.href = `SingleSchool/${schoolid}`;
    let route=`SingleSchool/${schoolid}`;
    routerStore.history.push(route);

    }
    @action UpdateFeature = (data) => {
     let c_id=data.id;
     let F_name=data.name;
DashboardStore.CurrentFeatureId=c_id;   
//  alert(c_id)

// DashboardStore.CurrentFeatureId=c_id;
DashboardStore.AddFeature['FeatureName']=F_name;

 
DashboardStore.ToggleModal(true);
    }

    @action Updateplan = (data) => {
     let c_id=data.id;
     let F_name=data.name;
     let feature_name=data.feature_name;
DashboardStore.CurrentPlanId=c_id;   
//  alert(c_id)
 // DashboardStore.CurrentFeatureId=c_id;
DashboardStore.AddNewPlan['PlanName']=F_name;

let Selected_features=JSON.parse(feature_name)
DashboardStore.AddNewPlan.Selected_features=Selected_features;
console.log(Selected_features);
// Features
let index = 1;
const formattedMessagedata = Selected_features.map(singleData => {
 
    return {   
       id:index++,
          value: singleData, 
         label: singleData,
         };
  });
  DashboardStore.AddNewPlan.Features=formattedMessagedata;

// Features:[],


DashboardStore.ToggleModal(true);
    }

    @action ResetFeatureForm = () => {
DashboardStore.CurrentFeatureId='';   
DashboardStore.AddFeature['FeatureName']='';

DashboardStore.CurrentPlanId='';   
DashboardStore.AddNewPlan['PlanName']='';
DashboardStore.AddNewPlan.Selected_features=[];
DashboardStore.AddNewPlan.Features=null;
}

    GetAllActiveSchool = () => {
        console.warn("cardrequest");
        DashboardStore._ToggleProcessing();

        axios.get("schools")
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);

                        return {                        
                            id:index++,
                            name: singleData.name,
                            SubDomain: `${singleData.webname}.scloudschool.com`,
                            Address: singleData.address,
                            date: Requestdate,
                            Email: singleData.email,
                            Description: singleData.description,
                            Action: <div className="displayCenterFlex">
                                <MDBIcon fa icon="edit" className="ml-2 lightweight" onClick={() => DashboardStore.navigate2school(singleData)} />
                                {/* <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /> */}
                                {/* <MDBIcon onClick={() => DashboardStore.ModalNewuser(singleData)} fa icon="fa-plus" className="ml-2 colred lightweight" />  <i className="fa fa-fw fa-plus faStyle" onClick={() => DashboardStore.ModalNewuser(singleData)} />*/}
                                </div> ,
                        };
                      });
console.log(formattedMessagedata);
                    this.AllActiveSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.AllActiveSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.AllActiveSchool = [];
                //  this._ToggleLoader(false)
            });
    }

    GetAllParentInSchool = () => {
        let SchoolId= DashboardStore.CurrentSchoolId;
        console.warn("GetAllParentInSchool");
        DashboardStore._ToggleProcessing();

        axios.get(`parents?school_id=${SchoolId}`)
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);
  let fullname=singleData.user['firstname'] +" " + singleData.user['lastname'];
                        return {  

                            id:index++,
                            name: fullname,
                            occupation:singleData.occupation,
                            Relationship: singleData.relationship,
                            email: singleData.user['email'],
                            address:singleData.user['address'],
                       };
                      });
console.log(formattedMessagedata);
                    this.ParentInSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.ParentInSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.ParentInSchool = [];
                //  this._ToggleLoader(false)
            });
    }
    GetAllStudentInSchool= () => {
        let SchoolId= DashboardStore.CurrentSchoolId;
        console.warn("GetAllStudentInSchool");
        DashboardStore._ToggleProcessing();

        axios.get(`students?school_id=${SchoolId}`)
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);
  let fullname=singleData.user['firstname'] +" " + singleData.user['lastname'];
                        return {  
                  

                            id:index++,
                            name: fullname,
                            address:singleData.user['address'],
                            gender: singleData.user['gender'],
                            class: singleData.classname['name'],
                            date:Requestdate,
                       };
                      });
console.log(formattedMessagedata);
                    this.StudentInSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.StudentInSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.StudentInSchool = [];
                //  this._ToggleLoader(false)
            });
    }
    GetAllStudentInSchool= () => {
        let SchoolId= DashboardStore.CurrentSchoolId;
        console.warn("GetAllStudentInSchool");
        DashboardStore._ToggleProcessing();

        axios.get(`students?school_id=${SchoolId}`)
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.createdAt);
  let fullname=singleData.user['firstname'] +" " + singleData.user['lastname'];
                        return {  
                  

                            id:index++,
                            name: fullname,
                            address:singleData.user['address'],
                            gender: singleData.user['gender'],
                            class: singleData.classname['name'],
                            date:Requestdate,
                       };
                      });
console.log(formattedMessagedata);
                    this.StudentInSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.StudentInSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.StudentInSchool = [];
                //  this._ToggleLoader(false)
            });
    }
    GetAllTeacherInSchool= () => {
        let SchoolId= DashboardStore.CurrentSchoolId;
        console.warn("GetAllStudentInSchool");
        DashboardStore._ToggleProcessing();

        axios.get(`teachers?school_id=${SchoolId}`)
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.user['createdAt']);
  let fullname=singleData.user['firstname'] +" " + singleData.user['lastname'];
                        return {  
                  

                            id:index++,
                            name: fullname,
                            address:singleData.user['address'],
                            gender: singleData.user['gender'],
                            class: '',
                            date:Requestdate,
                       };
                      });
console.log(formattedMessagedata);
                    this.TeacherInSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.TeacherInSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.TeacherInSchool = [];
                //  this._ToggleLoader(false)
            });
    }
    GetAllStaffsInSchool= () => {
        let SchoolId= DashboardStore.CurrentSchoolId;
        console.warn("GetAllStudentInSchool");
        DashboardStore._ToggleProcessing();

        axios.get(`staffs?school_id=${SchoolId}`)
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.user['createdAt']);
  let fullname=singleData.user['firstname'] +" " + singleData.user['lastname'];
                        return {  
                  

                            id:index++,
                            name: fullname,
                            address:singleData.user['address'],
                            gender: singleData.user['gender'],
                            department: singleData.department,
                            date:Requestdate,
                       };
                      });
console.log(formattedMessagedata);
                    this.StaffsInSchool = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.StaffsInSchool = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.StaffsInSchool = [];
                //  this._ToggleLoader(false)
            });
    }

    GetAllActiveStudent = () => {
        console.warn("GetAllActiveStudent");
        DashboardStore._ToggleProcessing();

        axios.get("students")
            .then(response => {
                DashboardStore._ToggleProcessing();
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    console.log(resp)
                    let check = resp.hasOwnProperty("total");
                    let data = null;
                    if (check === true) {
                        // alert(check)
                        console.log("here")
                        data = resp.data;
                        console.log(data);
                    }
                    else {
                       // alert(check)
                        data = resp;
                    }
                    let index = 1;
                    const formattedMessagedata = data.map(singleData => {
                        let Requestdate = ReadableDate(singleData.user['createdAt']);
                        let birthday = ReadableDate(singleData.user['birthday']);
console.log(singleData.user['firstname']);
                        return {                        
                            id:index++,
                             name: singleData.school_id,
                             gender: singleData.user['gender'],
                             admissionNo: singleData.admission_no,
                            Fullname: `${singleData.user['firstname']}  ${singleData.user['lastname']}`,
                            Address: singleData.user['address'],
                            date: Requestdate,
                            Class: singleData.classname['name'],
                            dob: birthday,
                        };
                      });
console.log(formattedMessagedata);
                    this.AllActiveStudent = formattedMessagedata

                    //       this._ToggleLoader(false)
                }
                else {
                    DashboardStore._ToggleProcessing();
                    console.warn("Error loading Active Schools");

                    this.AllActiveStudent = []
                }
                
            })
            .catch(error => {
                DashboardStore._ToggleProcessing();
                console.warn(`error ${error}`);
                this.AllActiveStudent = [];
                //  this._ToggleLoader(false)
            });
    }


    




 



     isJson(item) {
        item = typeof item !== "string"
            ? JSON.stringify(item)
            : item;
    
        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }
    
        if (typeof item === "object" && item !== null) {
            return true;
        }
    
        return false;
    }
     toCommaSeperated(data) {
        let result = data.map(function(val) {
            return val;
          }).join(',');

          return result;
    }
    

}

const DashboardStore = new Dashboard()
export default DashboardStore
