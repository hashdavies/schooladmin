import React, { Component } from 'react';
import { MDBDataTable, MDBContainer, MDBIcon, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader } from 'mdbreact';


import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
import fs from 'fs'
// const fs = require('fs')
import queryString from 'query-string'
import ls from 'local-storage';
import { ToastContainer, toast } from 'react-toastify';
import jwt_decode from 'jwt-decode';
import SimpleReactValidator from 'simple-react-validator';
import { arrayToObject } from '../dependency/UtilityFunctions';
import 'react-toastify/dist/ReactToastify.css';



class Account {
    @observable validatorgen = null;
    Innit(validator) {
        // this.validator = new SimpleReactValidator();
        AccountStore.validatorgen = validator;
    }
    @observable resetpwd = {
        email: ''
    }
    @observable loginInfo = {
        email: '',
        password: '',
        rememberme: false,
    }
    @observable createUser = {

    }
    @observable CurrentUserInfo = {

    }
    @observable loggedinUserdetails = {

    }
    @observable Modalopen = false;
    @observable updater = false
    @observable isformedit = false
    @observable disabled = false
    @observable loader_visible = false
    @observable redirectmode = false
    @observable isprocessing = false
    @observable redirectToReferrer = false
    @observable Allcountries = []
    @observable Allregions = []
    // @observable CorporateBranchUsers = []
    @observable Allregions2 = []
    @observable Allroles = []
    @observable Reg_BankAdmin = {
        "email": "",
        "lastName": "",
        "firstName": "",
        // "fullName": "Joshua Alonge",
        "phoneNumber": "",
        // "userName": "",
        "companyName": "",
        "country": {},
        "region": "",
        "role": "",
        "BankTitle": "",
        "BankRegion": "",
        "BankCountry": "",
        "address": "",
        "bankemail": "",
        "showfield": false,
        "imageFile": null,

    }
    // @observable CorporateBranchUsers = {

    //     rows: [
    //         {
    //         name: 'Ashton',
    //          email: 'test@gmail.com',
    //         phone: <span className="siteColor">+234567*******</span>,
    //         country: 'Lagos/Nigeria',
    //         role: 'User',
    //          action: <div className="displayCenterFlex"><MDBIcon fa icon="edit" className="ml-2 lightweight" /> <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /></div>
    //         },
    //         {
    //         name: 'Ashton',
    //          email: 'test@gmail.com',
    //         phone: <span className="siteColor">+234567*******</span>,
    //         country: 'Lagos/Nigeria',
    //         role: 'User',
    //          action: <div className="displayCenterFlex"><MDBIcon fa icon="edit" className="ml-2 lightweight" /> <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /></div>
    //         }
    //     ]
    //   };
    @observable CorporateBranchUsers = []


    @observable Reg_BankUser = {
        "email": "",
        "lastName": "",
        "firstName": "",
        "phoneNumber": "",
        "country": '',
        "region": '',
        "role": "",
        "corporateid": ""

    }

    @action _ToggleProcessing = () => {
        this.isprocessing = !this.isprocessing;
    }
    @action _ToggleFormProcess = (status) => {
        this.isformedit = status;
    }


    @action ToggleModal(reset = false) {
        let m = {
            "email": "",
            "lastName": "",
            "firstName": "",
            "phoneNumber": "",
            "country": '',
            "region": '',
            "role": "",
            "corporateid": ""

        }
        if (reset === true) {
            AccountStore._ToggleFormProcess(false);
        }

        AccountStore.Reg_BankUser = m;
        AccountStore.Modalopen = !AccountStore.Modalopen;
    }


    @action handlefilePicker(event, type) {
        // event.preventDefault();
        const name = event.target.name;
        console.log(event.target.files[0]);
        // AccountStore[type][name]=URL.createObjectURL(event.target.files[0])
        AccountStore[type][name] = event.target.files[0]
        // this.setState({
        //   file: URL.createObjectURL(event.target.files[0])
        // })
    }
    @action handleInputChange(e, type) {
        const value = e.target[e.target.type === "checkbox" ? "checked" : "value"]
        const name = e.target.name;
        console.log(name)
        console.log(type)
        AccountStore[type][name] = value;
    }

    @action _handleChangeSelect = (stateOption, name, type) => {
        let value = stateOption.value;
        console.log(stateOption.value)
        console.log(name)

        this[type][name] = stateOption;
        // console.log(`Option selected:`, selectedOption);
        if (name === 'country') {
            this.GetRegions(stateOption.value);
        }
        else if (name === 'BankCountry') {
            this.GetRegions2(stateOption.value);
        }
        else {
            console.log('no reques')
        }

    }
    @action _ToggleLoader = (status) => {
        this.loader_visible = status;
    }

    @action _LoginHandler = () => {
        // let Accesstoken = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IkRFM0FEN0JDODgyMzVGNEM1QzMwM0MxRjU0NUNBOTAwMkRCMjkwNzIiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiI2NGQ1NmM5Ny01NTExLTRlNTEtYjYyOS0wZGNjNDE4NjYwOTciLCJuYW1lIjoidG9tenlhZGV4QGdtYWlsLmNvbSIsIlZEQV8wMSI6IjMwMSIsIlZEQV8wMiI6IjMwMiIsIlZEQV8wMyI6IjMwMyIsIlZEQV8wNCI6IjMwNCIsIlZEQV8wNSI6IjMwNSIsInJvbGUiOiJCQU5LX0FETUlOIiwiQ29ycG9yYXRlSWQiOiI5NWQ4NWMzNC0xZjk4LTRkYjktOTdkMS1jNDEzNDUwY2FmYTgiLCJnaXZlbl9uYW1lIjoiQWRlcmFudGkgIHRvbWl3YSIsImF1ZCI6WyJCQU5LX0FETUlOIiwicmVzb3VyY2Vfc2VydmVyIl0sInRva2VuX3VzYWdlIjoiYWNjZXNzX3Rva2VuIiwianRpIjoiMDViZDdkYmQtMWE3Ny00ZWJkLWI3MmYtYzQzMThjNDgzMGQ1IiwiY2ZkX2x2bCI6InByaXZhdGUiLCJzY29wZSI6WyJvcGVuaWQiLCJlbWFpbCIsInByb2ZpbGUiLCJvZmZsaW5lX2FjY2VzcyIsInJvbGVzIl0sImF6cCI6ImNvbXBhbmlvbndlYmFwcCIsIm5iZiI6MTU1NTA3ODk2NywiZXhwIjoxNTU1MDgyNTY3LCJpYXQiOjE1NTUwNzg5NjcsImlzcyI6Imh0dHA6Ly9jb21wYW5pb25hcGkuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.ucUB72SLzQxEdlpkmtMniWs_aRc_2LC9dBcmw1rfnj8IFqRfCzVplY7T8JjaKvUX-dGqzfeSK0kSaqkXcVgEW11E7RjKi5AruTM31zOjNT6wRHe0Tpx4ezZYPqEcGBr2Rx3dQwlk-wO5mH7NfV0CYtBaX4qJHsIiBHy8-V2OJJXo_mDrluxCDNCVcDB3FBgJiC1NX3j9AKCSau31ByD15iB_9189w7UfIEx2a4ysNnFHu721pN8xknVr9ZMi7j5CIvuPx-5c1maDcVWDn5_iJest5snlM7sdgNkTHxIX-ZS8_AgYwZN6gibD2vpbijBDc1BAsiRkt7TsHnrhE-CyDA';
        // var decoded = jwt_decode(Accesstoken);
        // console.log(decoded);
        const { email, password, rememberme } = AccountStore.loginInfo;
        // alert(email);
        if (email === '') {
            toast.info("Email Address is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (password === '') {
            toast.info("Password Address is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else {

            let params = {
                grant_type: "password",
                password: password,
                // scope: 'openid profile email roles offline_access',
                username: email,
                client_id: "companionwebapp",
                client_secret: "c12edcdb-35c4-4a0d-9fe3-caf4174fe57c",
            }
            // const config = {
            //     headers: {
            //       "Content-Type": "application/x-www-form-urlencoded"
            //     }
            //   };
            console.log(params);
            this.isprocessing = true
            axios.post(`/connect/token`,
                queryString.stringify(params))
                .then(response => {
                    this.isprocessing = false
                    // console.log(response)
                    console.log(response.status)
                    if (response.status === 200) {
                        let resp = response.data;
                        console.log(resp)
                        let token_type = resp.token_type
                        let Accesstoken = resp.access_token
                        let expires_in = resp.expires_in
                        console.log(token_type)
                        var decoded = jwt_decode(Accesstoken);
                        console.log(decoded);
                        console.log(decoded.given_name);
                        this.loggedinUserdetails = decoded;
                        let data2save = {
                            isloggedin: true,
                            userid: decoded.SYS_01,
                            usertype: decoded.role,
                            fullname: decoded.given_name,
                            accessToken: Accesstoken,
                            expires_in: expires_in,
                            expires_in_timestap: decoded.exp
                        }
                        console.log(data2save)
                        //  let stringify_data = JSON.stringify(data);
                        ls.set('Loggedinstate', data2save);
                        ls.set('loggedinUserdetails', this.loggedinUserdetails);
                        this.redirectToReferrer = true;
                    }
                    else {
                        console.log(response)
                        this.isprocessing = false
                    }

                })
                .catch(err => {
                    this.isprocessing = false
                    toast.error('An Error Occured \n Please check your Cridentials', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }
    @action ModalNewuser = (data) => {
        //  AccountStore.Reg_BankUser=data;
        // let m= {
        //     "email": "",
        //     "lastName": "",
        //     "firstName": "",
        //     "phoneNumber": "",
        //     "country": '',
        //     "region": '',
        //     "role": "",
        //     "corporateid": ""

        // }

        // AccountStore.Reg_BankUser = m;
        AccountStore.ToggleModal();
        AccountStore._ToggleFormProcess(false);
        AccountStore.Reg_BankUser.corporateid = data.id;

    }
    @action ModalEditUser = (data) => {
        console.log(data);
        // alert(JSON.stringify(data));
        //    let m= {
        //         "email": "",
        //         "lastName": "",
        //         "firstName": "",
        //         "phoneNumber": "",
        //         "country": '',
        //         "region": '',
        //         "role": "",
        //         "corporateid": ""

        //     }

        //     AccountStore.Reg_BankUser = m;
        AccountStore.ToggleModal();
        AccountStore._ToggleFormProcess(true);
        AccountStore.Reg_BankUser = data;
        AccountStore.CurrentUserInfo = data;


        console.log(this.isformedit)
        console.log(AccountStore.Reg_BankUser)
    }
    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
        console.log(AccountStore.Reg_BankAdmin)
        const { email, lastName, firstName, phoneNumber, companyName, country, region, role, BankTitle, BankRegion, BankCountry, address, bankemail, showfield, imageFile
        } = AccountStore.Reg_BankAdmin;
        let _bankregion = '';
        let _bankcountry = '';
        // console.log(country.value);         

        if (firstName === '') {
            toast.info("First Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (lastName === '') {
            toast.info("Last Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (email === '') {
            toast.info("Email Address is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (phoneNumber === '') {
            toast.info("Phone Number  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (companyName === '') {
            toast.info("Company name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (country === '') {
            toast.info("Please Select a Country", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (region === '') {
            toast.info("Please Select a Region", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (role === '') {
            toast.info("Please Select a Role", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (BankTitle === '') {
            toast.info("Bank Title  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (address === '') {
            toast.info("Bank Address  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (bankemail === '') {
            toast.info("Bank Email  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else {
            if (showfield === true) {
                if (BankCountry === '') {
                    toast.info("Please Select a Bank Region", {
                        position: toast.POSITION.TOP_CENTER
                    });
                }
                else if (BankRegion === '') {
                    toast.info("Please Select a Region", {
                        position: toast.POSITION.TOP_CENTER
                    });
                }
                else {
                    _bankcountry = BankCountry.value;
                    _bankregion = BankRegion.value;
                }


            }
            else {
                _bankcountry = country.value;
                _bankregion = region.value;
            }

            let params = {
                email: email,
                lastName: lastName,
                firstName: firstName,
                fullName: firstName + '  ' + lastName,
                phoneNumber: phoneNumber,
                userName: email,
                companyName: companyName,
                country_Id: country.value,
                region_Id: region.value,
                // role: role.value,
                role: 'BANK_ADMIN',
                BankTitle: BankTitle,
                BankRegion_Id: _bankregion,
                BankCountry_Id: _bankcountry,
                address: address,
                bankemail: bankemail

            }
            // formData.append(item.name, fs.createReadStream(pathToFile));
            var bodyFormData = new FormData();
            bodyFormData.set('model', JSON.stringify(params));
            // bodyFormData.set('model', params);
            // bodyFormData.set('email', email);
            // bodyFormData.set('firstName', lastName);
            bodyFormData.append('File', imageFile);
            // bodyFormData.append('File', fs.createReadStream(imageFile));


            console.log(bodyFormData);
            // console.log(params);
            AccountStore.isprocessing = true
            // axios.post(`/api/UserManagement/createBankAdmin`,
            //     queryString.stringify(params))
            // axios.post(`api/UserManagement/createBankAdmin`,
            // queryString.stringify(params))

            const config = {
                headers: {
                    "Content-Type": "multipart/form-data"
                    // 'content-type': `multipart/form-data; boundary=${bodyFormData._boundary}`
                }
            };
            axios.post(`api/UserManagement/createBankAdmin`,
                bodyFormData, config)
                .then(response => {
                    AccountStore.isprocessing = false
                    console.log(response)
                    console.log(response.status)
                    AccountStore.isprocessing = false

                    if (response.status === 200) {
                        console.log(resp)
                        let resp = response.data;
                        let internalcode = resp.code;

                        let text = resp.description;
                        if (internalcode > 0) {
                            toast.success(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                            setTimeout(
                                function () {
                                    window.location.reload();

                                }, 2000);

                        }
                        else {
                            toast.error(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                        }




                    }
                    else {
                        console.log(response)
                        console.log('response')
                        AccountStore.isprocessing = false
                    }

                })
                .catch(err => {
                    AccountStore.isprocessing = false
                    toast.error('An Error Occured', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }



    onFormSubmit_ChangePwd(e) {
        e.preventDefault() // Stop form submit

        console.log(AccountStore.resetpwd)
        const { email } = AccountStore.resetpwd;

        if (AccountStore.validatorgen.allValid()) {
            // alert('You submitted the form and stuff!');

            // {
            //   "email": "string",
            //   "lastName": "string",
            //   "firstName": "string",
            //   "fullName": "string",
            //   "phoneNumber": "string",
            //   "userName": "string",
            //   "country_Id": "string",
            //   "region_Id": "string",
            //   "role": "string",
            //   "corporateId": "string"
            // }
            let params = {
                emailAddress: email
            }
            // const config = {
            //     headers: {
            //       "Content-Type": "application/x-www-form-urlencoded"
            //     }
            //   };
            console.log(params);
            AccountStore.isprocessing = true
            axios.post(`api/Auth/ForgotPassword`,
                queryString.stringify(params))
                .then(response => {
                    AccountStore.isprocessing = false
                    // console.log(response)
                    console.log(response.status)

                    if (response.status === 200) {
                        console.log(resp)
                        let resp = response.data;
                        let internalcode = resp.code;

                        let text = resp.description;
                        if (internalcode > 0) {
                            toast.success(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                            // setTimeout(
                            //     function () {
                            //         window.location.reload();

                            //     }, 2000);

                        }
                        else {
                            toast.error(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                        }




                    }
                    else {
                        console.log(response)
                        AccountStore.isprocessing = false
                    }

                })
                .catch(err => {
                    AccountStore.isprocessing = false
                    toast.error('An Error Occured \n Please Check the Email Address', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })

        }

        else {
            // alert("Not yet");
            AccountStore.validatorgen.showMessages();
            let Errors = AccountStore.validatorgen.getErrorMessages();
            console.log(Errors)
            AccountStore.updater = !AccountStore.updater;
            let ErrorKey = Object.keys(Errors)[0];
            let ErrorMessage = Errors[Object.keys(Errors)[0]];
            toast.warn(ErrorMessage, {
                position: toast.POSITION.TOP_CENTER
            });


        }

        // 
    }




    @action _addBankUser(e) {
        e.preventDefault()
        console.log(AccountStore.Reg_BankUser)

        const { email, lastName, firstName, phoneNumber, country, region, role,
        } = AccountStore.Reg_BankUser;
        // alert(firstName)
        // alert(phoneNumber)
        console.log(AccountStore.Reg_BankUser)

        if (firstName === '') {
            toast.warning("First Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (lastName === '') {
            toast.warning("Last Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (email === '') {
            toast.warning("Email Address is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (phoneNumber === '') {
            toast.warning("Phone Number  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (country === '') {
            toast.warning("Please Select a Country", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (region === '') {
            toast.warning("Please Select a Region", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (role === '') {
            toast.warning("Please Select a Role", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        // else if (corporate === '') {
        //     toast.warning("Corperate   is required", {
        //         position: toast.POSITION.TOP_CENTER
        //     });
        // }

        else {
            if (AccountStore.Reg_BankUser.corporateid === '') {
                AccountStore.Reg_BankUser.corporateid = '233c7cd9-4b50-4286-bd5a-b21afc40aa84';
            }

            let params = {
                // "email": email,
                // "lastName": lastName,
                // "firstName": firstName,
                // "fullName": firstName + ' ' + lastName,
                // "phoneNumber": phoneNumber,
                // "userName": email,
                // "country_Id": country.value,
                // "region_Id": region.value,
                // "role": role.value,
                // "loginEmail": email,
                // "corporateId": corporateid

                // "email": email,
                // "lastName": "joshua",
                // "firstName": "alonge",
                // "fullName": "joshua Alonge",
                // "phoneNumber": "08068674073",
                // "userName": email,
                // "country_Id": "F3C9FC6E-9B82-4CD2-A0E8-4A011AF1265C",
                // "region_Id": "22D3EEE6-D2E5-4DA9-8D34-E9821D05B78C",
                // "role": "BANK_USER",
                // "loginEmail": email,
                // "corporateId":"A5F6D4F7-98AB-404F-9E04-BE434E97F476"


                "email": email,
                "lastName": lastName,
                "firstName": firstName,
                "fullName": firstName + ' ' + lastName,
                "phoneNumber": phoneNumber,
                "userName": email,
                "country_Id": country.value,
                "region_Id": region.value,
                "role": role.value,
                "loginEmail": email,
                // "corporateId":corporateid
                "corporateId": AccountStore.Reg_BankUser.corporateid
            }

            console.log(params);
            AccountStore.isprocessing = true
            axios.post(`api/UserManagement/createBankuser`, params)
                .then(response => {
                    AccountStore.isprocessing = false
                    // console.log(response)
                    console.log(response.status)

                    if (response.status === 200) {
                        console.log(resp)
                        let resp = response.data;
                        let internalcode = resp.code;

                        let text = resp.description;
                        if (internalcode > 0) {
                            toast.success(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                            // setTimeout(
                            //     function () {
                            //         window.location.reload();

                            //     }, 2000);
                            AccountStore.ToggleModal();
                        }
                        else {
                            toast.error(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                        }




                    }
                    else {
                        console.log(response)
                        AccountStore.isprocessing = false
                    }

                })
                .catch(err => {
                    AccountStore.isprocessing = false
                    toast.error('An Error Occured \n Please Check the Email Address', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }

    @action onFormSubmit_update_User(e) {
        e.preventDefault()
        console.log('update field')

        const { email, lastName, firstName, phoneNumber, country, region, role,
        } = AccountStore.Reg_BankUser;
        console.log(typeof (country));
        let existing_Info = AccountStore.CurrentUserInfo;
        console.log(existing_Info);
        console.log('existing_Info');
        let country_Id = '';
        let region_Id = '';
        let corporateId = existing_Info.corporateId;
        let user_role = '';
        let my_id = existing_Info.userId;


        if (country === '' || typeof (country) === 'undefined') {
            country_Id = existing_Info.countryId;
        }
        else {
            country_Id = country.value

        }
        if (region === '' || typeof (region) === 'undefined') {
            region_Id = existing_Info.regionId;
        }
        else {
            region_Id = region.value;

        }
        if (role === '' || typeof (role) === 'undefined') {
            user_role = existing_Info.roleName;

        }
        else {
            user_role = role.value;

        }



        if (firstName === '') {
            toast.warning("First Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (lastName === '') {
            toast.warning("Last Name  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (email === '') {
            toast.warning("Email Address is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }
        else if (phoneNumber === '') {
            toast.warning("Phone Number  is required", {
                position: toast.POSITION.TOP_CENTER
            });
        }


        else {


            let params = {

                "id": my_id,
                "email": email,
                "lastName": lastName,
                "firstName": firstName,
                "fullName": firstName + ' ' + lastName,
                "phoneNumber": phoneNumber,
                "userName": email,
                "country_Id": country_Id,
                "region_Id": region_Id,
                "corporateId": corporateId,
                "role": user_role
            }

            console.log(params);
            console.log('params');
            AccountStore.isprocessing = true
            axios.post(`api/UserManagement/edituser`, params)
                .then(response => {
                    AccountStore.isprocessing = false
                    // console.log(response)
                    console.log(response.status)

                    if (response.status === 200) {
                        console.log(resp)
                        let resp = response.data;
                        let internalcode = resp.code;

                        let text = resp.description;
                        if (internalcode > 0) {
                            toast.success(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                            // setTimeout(
                            //     function () {
                            //         window.location.reload();

                            //     }, 2000);
                            AccountStore.getCoperateUsers();

                            AccountStore.ToggleModal();
                        }
                        else {
                            toast.error(text, {
                                position: toast.POSITION.TOP_CENTER
                            });
                        }




                    }
                    else {
                        console.log(response)
                        AccountStore.isprocessing = false
                    }

                })
                .catch(err => {
                    AccountStore.isprocessing = false
                    toast.error('An Error Occured \n Please Check the Email Address', {
                        position: toast.POSITION.TOP_CENTER
                    });
                    console.log(err)
                })
        }

        // 
    }

    getCoperateUsers = () => {
        console.warn("getCoperateUsers");

        AccountStore._ToggleProcessing();

        let params = {
            "fromDate": null,
            "toDate": null,
            "filterBy": null,
            "pageIndex": 1,
            "pageSize": 100
        }
        const config = {
            headers: {
                "Content-Type": "multipart/form-data"
                // 'content-type': `multipart/form-data; boundary=${bodyFormData._boundary}`
            }
        };
        axios.post("api/UserManagement/GetCorporateBranchUsers", params)
            .then(response => {
                console.log(response);
                AccountStore._ToggleProcessing();

                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp>>")
                    console.log(payload)
                    let index = 0;
                    const FormattedData = payload.map(singleData => {

                        return {
                            id: index += 1,
                            FullName: singleData.fullName,
                            EmailAddress: singleData.email,
                            PhoneNumber: <span className="siteColor">{singleData.phoneNumber}</span>,
                            CountryRegion: singleData.countryName,
                            Role: singleData.roleName,
                            // Action: <div className="displayCenterFlex"><MDBIcon fa icon="edit" className="ml-2 lightweight" onClick={() => AccountStore.ModalEditUser(singleData)} /> <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" /><MDBIcon onClick={() => AccountStore.ModalNewuser(singleData)} fa icon="plus" className="ml-2 colred lightweight" /></div>
                            Action: <div className="displayCenterFlex">
                                <MDBIcon fa icon="edit" className="ml-2 lightweight" onClick={() => AccountStore.ModalEditUser(singleData)} />
                                <MDBIcon fa icon="trash-alt" className="ml-2 colred lightweight" />
                                <MDBIcon onClick={() => AccountStore.ModalNewuser(singleData)} fa icon="fa-plus" className="ml-2 colred lightweight" />  <i className="fa fa-fw fa-plus faStyle" onClick={() => AccountStore.ModalNewuser(singleData)} /></div>

                        };
                    });
                    console.log("kkmama")
                    console.log(FormattedData);
                    console.log("kkmama")

                    // let reformated=arrayToObject(FormattedData);
                    // console.log(reformated)
                    console.log("kkmama2")







                    console.log(AccountStore.updater)
                    console.log('AccountStore.updater')
                    // AccountStore.CorporateBranchUsers['columns'] = thead;
                    AccountStore.CorporateBranchUsers = FormattedData;
                    // AccountStore.updater= !AccountStore.updater;
                    // console.log(AccountStore.updater)
                    // console.log('AccountStore.updater')
                    console.log(this.CorporateBranchUsers);
                    // AccountStore.updater= !AccountStore.updater;
                }
                else {

                    console.warn("Error loading corperate user");

                    // this.CorporateBranchUsers['rows'] = []

                }
            })
            .catch(error => {
                AccountStore._ToggleProcessing();

                console.warn(`error ${error}`);
                // this.CorporateBranchUsers['rows']  = [];
                //  this._ToggleLoader(false)
            });
    }
    Getcountry = () => {
        console.warn("country");


        axios.get("/api/Country/GetCountryNameAndId")
            .then(response => {
                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp")
                    console.log(payload)
                    const formattedMessagedata = payload.map(singleData => {

                        return {
                            value: singleData.country_Id,
                            label: singleData.countryName
                        };
                    });
                    console.log(formattedMessagedata);
                    this.Allcountries = formattedMessagedata

                }
                else {

                    console.warn("Error loading Country");

                    this.Allcountries = []

                }
            })
            .catch(error => {
                console.warn(`error ${error}`);
                this.Allcountries = [];
                //  this._ToggleLoader(false)
            });
    }
    GetRegions = (id) => {
        console.warn("country");

        //    this._ToggleLoader(true)
        axios.get(`/api/Country/GetRegionNameAndId?id=${id}`)
            .then(response => {

                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp")
                    console.log(payload)
                    const formattedMessagedata = payload.map(singleData => {

                        return {
                            value: singleData.region_Id,
                            label: singleData.regionName
                        };
                    });
                    console.log(formattedMessagedata);
                    this.Allregions = formattedMessagedata

                }
                else {

                    console.warn("Error loading Regions");

                    this.Allregions = []

                }
            })
            .catch(error => {
                console.warn(`error ${error}`);
                this.Allregions = [];
            });
    }
    GetRegions2 = (id) => {
        console.warn("country");

        //    this._ToggleLoader(true)
        axios.get(`/api/Country/GetRegionNameAndId?id=${id}`)
            .then(response => {

                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp")
                    console.log(payload)
                    const formattedMessagedata = payload.map(singleData => {

                        return {
                            value: singleData.region_Id,
                            label: singleData.regionName
                        };
                    });
                    console.log(formattedMessagedata);
                    this.Allregions2 = formattedMessagedata

                }
                else {

                    console.warn("Error loading Regions");

                    this.Allregions2 = []

                }
            })
            .catch(error => {
                console.warn(`error ${error}`);
                this.Allregions2 = [];
            });
    }
    GetRoles = () => {
        console.warn("Roles>>>>>>>>>");

        //    this._ToggleLoader(true)
        axios.post(`/api/UserManagement/GetRolesNameAndId`)
            .then(response => {

                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp")
                    console.log(payload)
                    const formattedMessagedata = payload.map(singleData => {

                        return {
                            value: singleData.id,
                            label: singleData.name
                        };
                    });
                    console.log(formattedMessagedata);
                    this.Allroles = formattedMessagedata

                }
                else {

                    console.warn("Error loading roles");

                    this.Allroles = []

                }
            })
            .catch(error => {
                console.warn(`error ${error}`);
                this.Allroles = [];
            });
    }

    GetRoles_Bankuser = () => {
        console.warn("Roles>>>>>>>>>");

        //    this._ToggleLoader(true)
        axios.post(`/api/UserManagement/GetRolesNameAndId`)
            .then(response => {

                console.log(response);
                if (response.status === 200) {
                    let resp = response.data;
                    // console.warn(resp)
                    let payload = resp.payload
                    console.log("ppp")
                    console.log(payload)
                    const formattedMessagedata = payload.map(singleData => {

                        return {
                            value: singleData.name,
                            label: singleData.name
                        };
                    });
                    console.log(formattedMessagedata);
                    this.Allroles = formattedMessagedata

                }
                else {

                    console.warn("Error loading roles");

                    this.Allroles = []

                }
            })
            .catch(error => {
                console.warn(`error ${error}`);
                this.Allroles = [];
            });
    }













 
}

const AccountStore = new Account()
export default AccountStore
